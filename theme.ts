import { extendTheme } from "@chakra-ui/react";

const fonts = {
    heading: 'var(--font-dm_sans)',
    body: 'var(--font-dm_sans)',
}

const colors = {
    'primary': '#307FE2',
    'abu-abu': '#F4F4F4',
    'green': '#0E5C08',
    'light-green': '#0E5C0842',
    'muted': '#BDB8B8',
    'red': '#EF4486',
    'light-red': '#EF448642',
}

export const theme = extendTheme({ fonts, colors })