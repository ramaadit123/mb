import { Box, Divider, Flex, SimpleGrid, Text } from "@chakra-ui/react";
import Image from "next/image";
import App from "../layouts/app";
import CategoryMenu from "@/components/molecules/CategoryMenu";
import CategoryItems from "@/components/molecules/CategoryItems";

export default function index() {
    return (
        <App title='Daftar Kategori'>
            <Box pl={{ base: '5', md: '20', xl: '20' }} pr={{ base: ' 10', md: '44', xl: '44' }}>
                <Text fontSize='3xl' fontWeight='bold' mb='5'>Kategori</Text>
                <Divider orientation="horizontal" borderColor='gray.300' borderWidth='1px' />

                <Flex ml={{ base: '5', md: '10', xl: '10' }} gap='5' wrap={{ base: 'wrap', md: 'nowrap', xl: 'nowrap' }}>
                    <Box display='block' width={{ base: 'full', md: '20%', xl: '20%' }} mt='5' >
                        <CategoryMenu name="Top Up Digital" />
                        <CategoryMenu name="Akun Premium" />
                        <CategoryMenu name="PPOB" />
                    </Box>

                    <Box height='-webkit-fit-content'>
                        <Divider orientation="vertical" borderColor='gray.300' borderWidth='1px' />
                    </Box>

                    <Box display='block' mt='5' pb='20'>
                        <Text fontSize={{ base: '2xl', md: '3xl', xl: '3xl' }} fontWeight='bold'>Semua di Top Up Digital</Text>
                        <SimpleGrid columns={{ base: 1, md: 3, xl: 3 }} spacing={10} mt='5'>
                            <CategoryItems href='#' image='/images/products/game.png' name="Diamond Mobile Legends" />
                            <CategoryItems href='#' image='/images/products/game.png' name="Diamond Mobile Legends" />
                            <CategoryItems href='#' image='/images/products/game.png' name="Diamond Mobile Legends" />
                            <CategoryItems href='#' image='/images/products/game.png' name="Diamond Mobile Legends" />
                            <CategoryItems href='#' image='/images/products/game.png' name="Diamond Mobile Legends" />
                            <CategoryItems href='#' image='/images/products/game.png' name="Diamond Mobile Legends" />
                            <CategoryItems href='#' image='/images/products/game.png' name="Diamond Mobile Legends" />
                            <CategoryItems href='#' image='/images/products/game.png' name="Diamond Mobile Legends" />
                            <CategoryItems href='#' image='/images/products/game.png' name="Diamond Mobile Legends" />
                        </SimpleGrid>
                    </Box>
                </Flex>
            </Box>
        </App>
    )
}
