import { Box, Container, Divider, Flex, Highlight, Image, SimpleGrid, Text } from "@chakra-ui/react";
import App from "../layouts/app";
import InputSelect from "@/components/molecules/InputSelect";
import { BoxDeliver, TextPrice } from "@/components/atoms";
import { IconTruckBlue } from "@/components/icons";

export default function Riwayat() {
    return (
        <App title='Riwayat Pembelian'>
            <Container maxW='8xl'>
                <Box>
                    <InputSelect text="Riwayat Pembelian" height="14">
                        <option value="Riwayat Pembelian">Riwayat Pembelian</option>
                    </InputSelect>
                </Box>
                <Box>
                    <Flex justifyContent='space-between' py='8'>
                        <Flex gap='5'>
                            <Box>
                                <Image
                                    borderRadius='lg'
                                    boxSize='130px'
                                    w='28'
                                    h='24'
                                    objectFit='cover'
                                    src='/images/products/mobile-legends.png'
                                    alt=''
                                />
                            </Box>
                            <Box>
                                <Text fontSize='xl' fontWeight='bold'>Top up 20 Diamond</Text>
                                <Text fontSize='sm' color='gray.400' fontStyle='italic'>Mobile Legends</Text>
                            </Box>
                        </Flex>
                        <Box textAlign='end'>
                            <BoxDeliver text='Menunggu Pembayaran' />
                            <TextPrice fontSize="xl" price="28. 000" />
                            <Text>X 1</Text>
                        </Box>
                    </Flex>

                    <Divider border='1px' borderColor='gray.300' />

                    <Flex alignItems='center' justifyContent='space-between' py='4' px='4'>
                        <Text fontSize='lg' color='gray.600'>1 Product</Text>
                        <Flex alignItems='center' gap='2'>
                            <Text fontSize='lg' color='gray.600'>Total Pesanan:</Text>
                            <TextPrice fontSize="lg" price='28.000' />
                        </Flex>
                    </Flex>

                    <Divider border='1px' borderColor='gray.300' />

                    <Flex alignItems='center' gap='4' py='4' px='4'>
                        <IconTruckBlue />
                        <Text fontSize='xl' color='primary'>Pesanan belum dibayar</Text>
                    </Flex>
                </Box>
            </Container>
        </App>
    )
}
