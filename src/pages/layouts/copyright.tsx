import { Box, Text } from "@chakra-ui/react";

export default function copyright() {
    return (
        <Box bg='black' py='2'>
            <Text textAlign='center' fontSize='xl' fontWeight='semibold' color='white'>
                &copy; 2023 - Maubang
            </Text>
        </Box>
    )
}
