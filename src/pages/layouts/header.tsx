import NextLink from "next/link";
import { ButtonPrimary, DividerCustom, NavLink } from "@/components/atoms";
import { IconBell, IconCategory, IconChart, IconSearch } from "@/components/icons";
import { Box, Divider, Flex, Image, Input, InputGroup, InputLeftElement, Link, Text } from "@chakra-ui/react";

export default function Header() {
    return (
        <>
            <Box bgColor='abu-abu' py={1}>
                <Flex alignItems='center' wrap={{ base: 'wrap', md: 'nowrap', lg: 'nowrap' }} justifyContent={{ base: 'center', md: 'center', lg: 'end' }} px={{ md: '44', lg: '44' }} gap={{ base: '3', md: '10', lg: '10' }}>
                    <NavLink href="#" fontSize={{ base: 'sm', md: 'md', lg: 'md' }} color="primary" fontWeight="semibold">Layanan</NavLink>
                    <NavLink href="#" fontSize={{ base: 'sm', md: 'md', lg: 'md' }} color="primary" fontWeight="semibold">Cara Pesan</NavLink>
                    <NavLink href="#" fontSize={{ base: 'sm', md: 'md', lg: 'md' }} color="primary" fontWeight="semibold">Testimoni</NavLink>
                    <NavLink href="#" fontSize={{ base: 'sm', md: 'md', lg: 'md' }} color="primary" fontWeight="semibold">FAQ</NavLink>
                    <NavLink href="#" fontSize={{ base: 'sm', md: 'md', lg: 'md' }} color="primary" fontWeight="semibold">Laporan Kendala</NavLink>
                </Flex>
            </Box>

            <Box pl={{ base: '5', xl: '20' }} pr={{ base: '4', md: '44', xl: '44' }}>
                <Flex alignItems='center' justifyContent={{ base: 'space-between', md: 'normal', xl: 'normal' }} gap={{ base: '5', xl: '10' }} wrap={{ base: 'wrap', md: 'nowrap', xl: 'nowrap' }}>
                    <Image
                        src="/images/logos/maubang-header.png"
                        width={{ base: '30', xl: '30' }}
                        height={{ base: '20', xl: '20' }}
                        objectFit='cover'
                        alt="Logo"
                    />
                    <Link href='/category' as={NextLink} _hover={{ textDecoration: "none" }} display='flex' alignItems='center' gap='2'>
                        <IconCategory />
                        <Text fontSize='lg' fontWeight='semibold' color='primary'>Kategori</Text>
                    </Link>
                    <InputGroup>
                        <InputLeftElement pointerEvents='none'>
                            <IconSearch />
                        </InputLeftElement>
                        <Input type='text' placeholder='Coba cari Chip, Top Up, Pulsa...' />
                    </InputGroup>
                    <Box display='flex' alignItems='center' gap='5'>
                        <Box width={{ base: '10', xl: '8' }} height={{ base: '10', xl: '8' }}>
                            <IconChart />
                        </Box>
                        <Divider orientation='vertical' height={'50px'} borderWidth={'1px'} />
                        <Box width={{ base: '10', xl: '7' }} height={{ base: '10', xl: '7' }}>
                            <IconBell />
                        </Box>
                        <Divider orientation='vertical' height={'50px'} borderWidth={'1px'} />
                    </Box>
                    <Link href='/auth/login' as={NextLink}>
                        <ButtonPrimary width="52" size="lg">Login</ButtonPrimary>
                    </Link>
                </Flex>
            </Box>
            <Box py='5'>
                <DividerCustom />
            </Box>
        </>
    )
}
