import { DividerCustom } from "@/components/atoms";
import { IconClock, IconEnvelop, IconInstagram, IconLinkedin, IconPhone, IconTwitter } from "@/components/icons";
import { Box, Container, Flex, SimpleGrid, Text } from "@chakra-ui/react";
import Image from "next/image";
import { Fragment } from "react";

export default function Footer() {
    return (
        <Fragment>
            <DividerCustom />

            <Box px={{ base: '1', md: '20', xl: '20' }} pb={{ base: 10, md: 64, xl: 80 }} pt={5}>
                <SimpleGrid columns={{ base: 0, md: 4, xl: 4 }} spacing={{ base: '3', md: '10', xl: '10' }}>
                    <Box height={{ base: '', md: '80px', xl: '80px' }}>
                        <Image
                            src='/images/logos/maubang-footer.png'
                            alt="Maubang"
                            width='300'
                            height='80'
                            style={{ objectFit: 'cover' }}
                        />
                        <Text fontSize='xl' ml='5'>
                            Platform pertama di Indonesia yang memberikan layanan Berlangganan Bersama agar dapat menikmati fitur premium dengan proses berlangganan yang praktis, legal, aman dan murah
                        </Text>
                        <Flex alignItems='center' ml='5' gap='3' pt='3'>
                            <IconInstagram />
                            <IconTwitter />
                            <IconLinkedin />
                        </Flex>
                    </Box>
                    <Box height={{ base: '', md: '80px', xl: '80px' }} px={{ base: '5' }}>
                        <Text fontSize='4xl' fontWeight='bold' color='primary' mt='3'>
                            Maubang
                        </Text>
                        <Box display='block' pt='3'>
                            <Text fontSize='xl' mb='3'>Tentang Kami</Text>
                            <Text fontSize='xl' mb='3'>Cara Pesan</Text>
                            <Text fontSize='xl' mb='3'>Syarat dan Ketentuan</Text>
                            <Text fontSize='xl' mb='3'>Kebijakan Privasi</Text>
                            <Text fontSize='xl' mb='3'>Laporan Kendala</Text>
                            <Text fontSize='xl' mb='3'>Karir</Text>
                        </Box>
                    </Box>
                    <Box height={{ base: '', md: '80px', xl: '80px' }} px={{ base: '5' }}>
                        <Text fontSize='4xl' fontWeight='bold' color='primary' mt='3'>
                            Produk
                        </Text>
                        <Box display='block' pt='3'>
                            <Text fontSize='xl' mb='3'>Produk Digital</Text>
                            <Text fontSize='xl' mb='3'>Produk non Digital</Text>
                            <Text fontSize='xl' mb='3'>Program Patungan</Text>
                            <Text fontSize='xl' mb='3'>Program Sekeranjang</Text>
                            <Text fontSize='xl' mb='3'>Sequrban</Text>
                        </Box>
                    </Box>
                    <Box height={{ base: '', md: '80px', xl: '80px' }} px={{ base: '5' }}>
                        <Text fontSize='4xl' fontWeight='bold' color='primary' mt='3'>
                            Hubungi Kami
                        </Text>
                        <Box display='block' pt='3'>
                            <Flex alignItems='center' gap='2' mb='3'>
                                <IconEnvelop />
                                <Text fontSize='xl'>cs@maubang.id</Text>
                            </Flex>
                            <Flex alignItems='center' gap='2' mb='3'>
                                <IconPhone />
                                <Text fontSize='xl'>+62812 3456 7890</Text>
                            </Flex>
                            <Flex alignItems='center' gap='2'>
                                <IconClock />
                                <Text fontSize='xl' fontWeight='semibold'>Jam Operasional</Text>
                            </Flex>
                            <Text fontSize='xl' ml='8'>Setiap Hari</Text>
                            <Text fontSize='xl' ml='8'>09.00 - 21.00 WIB</Text>
                        </Box>
                    </Box>
                </SimpleGrid>
            </Box>
        </Fragment >
    )
}
