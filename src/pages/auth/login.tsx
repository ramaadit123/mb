import { ButtonOutlineIcon } from "@/components/atoms";
import { IconEmail, IconGoogle } from "@/components/icons";
import { Box, Container, Flex, Link, Text } from "@chakra-ui/react";
import { useRouter } from "next/router";
import App from "../layouts/app";
import NextLink from "next/link";
import Image from "next/image";

export default function login() {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const router = useRouter()

    return (
        <App title='Login'>
            <Container maxW={{ base: '7xl', md: '8xl', xl: '8xl' }} pb='10'>
                <Flex flexDirection="column" alignItems='center'>
                    <Image
                        src='/images/vectors/vector-login.png'
                        alt=''
                        width='500'
                        height='500'
                        style={{ objectFit: 'cover' }}
                    />
                    <Box mb='5'>
                        <ButtonOutlineIcon width={{ base: 'sm', md: '', xl: '' }} icon={<IconGoogle />}>Login dengan Google</ButtonOutlineIcon>
                    </Box>
                    <Box mb='5'>
                        <ButtonOutlineIcon width={{ base: 'sm', md: '', xl: '' }} icon={<IconEmail />} onClick={() => router.push('/auth/email')}>Login dengan email</ButtonOutlineIcon>
                    </Box>
                    <Text fontSize='lg' fontWeight='semibold'>
                        Belum punya akun?{' '}
                        <Link href="/auth/register" as={NextLink} color='primary'>
                            Daftar
                        </Link>
                    </Text>
                </Flex>
            </Container>
        </App>
    )
}
