import { ButtonOutlineIcon, ButtonPrimary } from "@/components/atoms";
import { IconGoogle } from "@/components/icons";
import { AbsoluteCenter, Box, Center, Container, Divider, FormControl, FormLabel, Input, Link, Text } from "@chakra-ui/react";
import NextLink from "next/link";
import App from "../layouts/app";

export default function register() {
    return (
        <App title='Pendaftaran Akun'>
            <Container maxW={{ base: '7xl', md: '8xl', xl: '8xl' }} pb='10'>
                <Center mb='5'>
                    <ButtonOutlineIcon width='xl' icon={<IconGoogle />}>Daftar dengan Google</ButtonOutlineIcon>
                </Center>
                <Center>
                    <Box position='relative' padding='10' w='2xl'>
                        <Divider />
                        <AbsoluteCenter bg='white' px='4'>
                            Atau login dengan
                        </AbsoluteCenter>
                    </Box>
                </Center>
                <Center>
                    <Box width={{ base: 'full', md: 'max-content', xl: 'max-content' }}>
                        <FormControl w='xl'>
                            <Box mb='8'>
                                <FormLabel fontSize='lg'>Email</FormLabel>
                                <Input type='email' variant='flushed' />
                            </Box>
                            <Box mb='8'>
                                <FormLabel fontSize='lg'>Password</FormLabel>
                                <Input type='password' variant='flushed' />
                            </Box>
                            <Box mb='8'>
                                <FormLabel fontSize='lg'>Kode Referal (optional)</FormLabel>
                                <Input type='text' variant='flushed' />
                            </Box>
                        </FormControl>
                    </Box>
                </Center>
                <Center mt='8'>
                    <ButtonPrimary size='xl' width='xl' py='3'>
                        <Text fontSize='xl'>Daftar</Text>
                    </ButtonPrimary>
                </Center>
                <Center pt='8'>
                    <Box>
                        <Text mb='8' textAlign='center'>
                            Dengan mendaftar saya menyetujui {" "}
                            <Link as={NextLink} href="#" color='primary'>Aturan Penggunaan</Link> dan{" "}
                            <Link as={NextLink} href="#" color="primary">Kebijakan Privasi Maubang</Link>
                        </Text>
                        <Text mb='8' textAlign='center' fontWeight='semibold'>
                            Sudah punya akun?{' '}
                            <Link as={NextLink} href="/auth/login" color='primary'>
                                Login
                            </Link>
                        </Text>
                    </Box>
                </Center>
            </Container>
        </App>
    )
}
