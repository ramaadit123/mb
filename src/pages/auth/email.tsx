import { ButtonOutlineIcon, ButtonPrimary } from "@/components/atoms";
import { IconGoogle } from "@/components/icons";
import { AbsoluteCenter, Box, Center, Container, Divider, FormControl, FormLabel, Input, Link, Text } from "@chakra-ui/react";
import NextLink from "next/link";
import App from "../layouts/app";
// import App from "../layouts/app";

export default function email() {
    return (
        <App title='Login dengan email'>
            <Container maxW={{ base: '7xl', md: '8xl', xl: '8xl' }} pb='10'>
                <Center>
                    <Box width={{ base: 'full', md: 'max-content', xl: 'max-content' }}>
                        <FormControl w='xl'>
                            <Box mb='8'>
                                <FormLabel fontSize='lg'>Email</FormLabel>
                                <Input type='email' variant='flushed' />
                            </Box>
                            <Box mb='8'>
                                <FormLabel fontSize='lg'>Password</FormLabel>
                                <Input type='password' variant='flushed' />
                            </Box>
                        </FormControl>
                    </Box>
                </Center>
                <Center>
                    <Link href="#" as={NextLink} fontSize='lg' color='primary' fontWeight='semibold'>Lupa Password?</Link>
                </Center>
                <Center mt='8'>
                    <ButtonPrimary size='xl' width='xl' py='3'>
                        <Text fontSize='xl'>Login</Text>
                    </ButtonPrimary>
                </Center>
                <Center>
                    <Box position='relative' padding='10' w='2xl'>
                        <Divider />
                        <AbsoluteCenter bg='white' px='4'>
                            Atau login dengan
                        </AbsoluteCenter>
                    </Box>
                </Center>
                <Center mb='8'>
                    <ButtonOutlineIcon width='xl' icon={<IconGoogle />}>Login dengan Google</ButtonOutlineIcon>
                </Center>
                <Center>
                    <Text fontSize='lg' fontWeight='semibold'>
                        Belum punya akun?{' '}
                        <Link as={NextLink} href="/auth/register" color='primary'>
                            Daftar
                        </Link>
                    </Text>
                </Center>
            </Container>
        </App>
    )
}
