import { Box, Button, Checkbox, Container, Divider, Flex, Image, Spacer, Stack, Text } from "@chakra-ui/react";
import App from "../layouts/app";
import { ButtonPrimary, TextPrice } from "@/components/atoms";
import { IconArrowRight, IconMasterCard, IconTestArrowRight } from "@/components/icons";
import IconInformationCircle from "@/components/icons/IconInformationCircle";

export default function Checkout() {
    return (
        <App title='Checkout'>
            <Container maxW='8xl' pb='20'>
                <Flex justifyContent='space-between'>
                    <Box width='full' mr='10'>
                        <Text fontSize='3xl' fontWeight='bold' mb='8'>Pembayaran</Text>
                        <Flex justifyContent='space-between' mb='8'>
                            <Flex gap='5'>
                                <Box>
                                    <Image
                                        borderRadius='lg'
                                        boxSize='130px'
                                        w='28'
                                        h='24'
                                        objectFit='cover'
                                        src='/images/products/mobile-legends.png'
                                        alt=''
                                    />
                                </Box>
                                <Box>
                                    <Text fontSize='2xl' fontWeight='bold' mb='2'>Top Up 20 Diamond</Text>
                                    <Text fontSize='md' color='gray.400' fontStyle='italic'>Mobile Legends</Text>
                                </Box>
                            </Flex>
                            <Box>
                                <TextPrice price="28.000" fontSize="2xl" />
                                <Text fontSize='xl' textAlign='end'>x 1</Text>
                            </Box>
                        </Flex>
                        <Box bg='gray.200' my='5' py='1'></Box>
                        <Box>
                            <Text fontSize='2xl' fontWeight='bold' mb='5'>Metode Pembayaran</Text>
                            <Flex justifyContent='space-between'>
                                <Flex alignItems='center' gap='5'>
                                    <IconInformationCircle />
                                    <Text fontSize='xl'>Belum pilih metode pembayaran</Text>
                                </Flex>
                                <Button variant='outline' color='primary' rounded='lg' border='1px' borderColor='primary' size={"lg"} fontSize='xl' fontWeight='bold'>Pilih Metode</Button>
                            </Flex>
                        </Box>
                        <Box bg='gray.200' my='5' py='1'></Box>
                        <Box mb='8'>
                            <Text fontSize='2xl' fontWeight='bold' mb='5'>Layanan Premium</Text>
                            <Checkbox size='lg'>
                                <Text fontSize='xl' fontWeight='bold'>Layanan Premium</Text>
                            </Checkbox>
                            <Stack pl='7' mt='1' spacing={1}>
                                <Text fontSize='lg'>Rp5.000 / Transaksi</Text>
                            </Stack>
                        </Box>
                        <Box p='5' bg='blue.50' border='1px' borderColor='blue' rounded='xl'>
                            <Text fontSize='xl'>
                                Dengan mengaktifkan layanan premium, kamu akan mendapatkan token bermain di itemku Play dan telah menyetujui syarat dan ketentuan layanan premium.
                            </Text>
                            <Text mt='5' textAlign='end' fontSize='xl' fontWeight='semibold' color='primary'>Tentang Layanan Premium</Text>
                        </Box>
                    </Box>

                    <Box rounded='xl' border='1px' borderColor='black' p='5' bg='white' height='max-content' className="shadow-checkout-box">
                        <Box rounded='xl' className='shadow-checkout' p='5' width='400px' mb='5'>
                            <Flex gap='5' alignItems='center'>
                                <IconMasterCard />
                                <Box>
                                    <Text fontSize='md' fontWeight='bold'>Makin hemat pakai kupon</Text>
                                    <Text fontSize='sm'>Yuk pakai kupon sebelum hangus</Text>
                                </Box>
                                <Text fontSize='2xl' fontWeight='bold' ml='5'>{'>'}</Text>
                            </Flex>
                        </Box>
                        <Box>
                            <Divider orientation="horizontal" borderColor='gray.300' my='2' />
                            <Text fontSize='2xl' fontWeight='bold'>Detail Pembayaran</Text>
                            <Divider orientation="horizontal" borderColor='gray.300' my='2' />
                            <Flex alignItems='center' justifyContent='space-between'>
                                <Box>
                                    <Text fontSize='xl'>Total Pesanan</Text>
                                    <Text fontSize='xl'>Layanan Premium</Text>
                                </Box>
                                <Box>
                                    <Text fontSize='xl'>Rp28.000</Text>
                                    <Text fontSize='xl'>Rp28.000</Text>
                                </Box>
                            </Flex>
                            <Divider orientation="horizontal" borderColor='gray.300' my='2' />
                            <Flex alignItems='center' justifyContent='space-between'>
                                <Text fontSize='xl' fontWeight='bold'>Total Pembayaran</Text>
                                <Text fontSize='xl' fontWeight='bold'>Rp33.000</Text>
                            </Flex>
                            <Box mt='5'>
                                <ButtonPrimary width="full" height='12'><Text fontSize='xl'>Bayar</Text></ButtonPrimary>
                            </Box>
                        </Box>
                    </Box>
                </Flex>
            </Container>
        </App>
    )
}