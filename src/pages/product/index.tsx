import { TextBreadcrumb } from "@/components/atoms";
import { IconFilter, IconSort } from "@/components/icons";
import CardProduct from "@/components/molecules/CardProduct";
import { Box, Breadcrumb, BreadcrumbItem, BreadcrumbLink, Button, Container, Flex, FormControl, FormLabel, Grid, Input, Modal, ModalBody, ModalCloseButton, ModalContent, ModalHeader, ModalOverlay, Text, useDisclosure } from "@chakra-ui/react";
import NextLink from "next/link";
import { useRef } from "react";
import App from "../layouts/app";
import InputSelect from "@/components/molecules/InputSelect";

export default function Product() {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const finalRef = useRef(null)

    return (
        <App title='Daftar Produk'>
            <Container maxW={{ base: '8xl', xl: '7xl' }}>
                <Flex alignItems='center' justifyContent='space-between'>
                    <Breadcrumb spacing='8px' separator=">" mb='3'>
                        <BreadcrumbItem>
                            <BreadcrumbLink href='/' as={NextLink}>
                                <TextBreadcrumb text="Beranda" />
                            </BreadcrumbLink>
                        </BreadcrumbItem>

                        <BreadcrumbItem isCurrentPage>
                            <BreadcrumbLink href='#'>
                                <TextBreadcrumb isCurrentPage={true} text="Product Keseluruhan" />
                            </BreadcrumbLink>
                        </BreadcrumbItem>
                    </Breadcrumb>

                    <Box>
                        <Button variant='outline' mx='3' borderColor='primary' color='primary' rounded='xl' width='200px' height='60px' rightIcon={<IconSort />} iconSpacing='20' fontSize='xl' fontWeight='semibold'>
                            Terlaris
                        </Button>
                        <Button variant='outline' mx='3' borderColor='primary' color='primary' rounded='xl' width='200px' height='60px' rightIcon={<IconSort />} iconSpacing='20' fontSize='xl' fontWeight='semibold'>
                            Harga
                        </Button>
                        <Button variant='outline' mx='3' borderColor='primary' color='primary' rounded='xl' width='200px' height='60px' leftIcon={<IconFilter />} iconSpacing='3' fontSize='xl' fontWeight='semibold' onClick={onOpen}>
                            Filter
                        </Button>
                    </Box>
                </Flex>
                <Box mt='20'>
                    <Grid templateColumns='repeat(4, 1fr)' gap={6} mb='24' justifyItems='center'>
                        <CardProduct href='/product/1' image="/images/products/Youtube.png" category="Youtube" title="User Premium 1 Bulan" description="User Premium" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/Youtube.png" category="Youtube" title="User Premium 1 Bulan" description="User Premium" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/Youtube.png" category="Youtube" title="User Premium 1 Bulan" description="User Premium" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/Youtube.png" category="Youtube" title="User Premium 1 Bulan" description="User Premium" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/game.png" category="Mobile Legends" title="20 Diamond" description="Top Up Diamond" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/game.png" category="Mobile Legends" title="20 Diamond" description="Top Up Diamond" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/game.png" category="Mobile Legends" title="20 Diamond" description="Top Up Diamond" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/game.png" category="Mobile Legends" title="20 Diamond" description="Top Up Diamond" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/phone.png" category="Paket Data" title="Unlimitd Harian 7 Hari" description="Paket Data Smartfren" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/phone.png" category="Paket Data" title="Unlimitd Harian 7 Hari" description="Paket Data Smartfren" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/phone.png" category="Paket Data" title="Unlimitd Harian 7 Hari" description="Paket Data Smartfren" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                        <CardProduct href='/product/1' image="/images/products/phone.png" category="Paket Data" title="Unlimitd Harian 7 Hari" description="Paket Data Smartfren" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                    </Grid>
                </Box>

                <Modal finalFocusRef={finalRef} isOpen={isOpen} onClose={onClose} size='xl'>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>
                            <Text fontSize='2xl' fontWeight='bold'>Filter Berdasarkan</Text>
                        </ModalHeader>
                        <ModalCloseButton />
                        <ModalBody pb='12'>
                            <Flex alignItems='center' justifyContent='space-between' gap='10' mb='5'>
                                <Text fontSize='lg'>Kategori / Tipe Produk</Text>
                                <InputSelect text="Tipe Produk" width="60" height="14">
                                    <option value="produk">Produk</option>
                                </InputSelect>
                            </Flex>
                            <Flex alignItems='center' justifyContent='space-between' gap='10' mb='5'>
                                <Text fontSize='lg'>Harga Minimal</Text>
                                <FormControl mb='4' width='60'>
                                    <Input type='text' variant='flushed' placeholder='1.000.000' fontSize='xl' fontWeight='semibold' borderBottom='1px' />
                                </FormControl>
                            </Flex>
                            <Flex alignItems='center' justifyContent='space-between' gap='10' mb='5'>
                                <Text fontSize='lg'>Harga Maximal</Text>
                                <FormControl mb='4' width='60'>
                                    <Input type='text' variant='flushed' placeholder='1.000.000' fontSize='xl' fontWeight='semibold' borderBottom='1px' />
                                </FormControl>
                            </Flex>
                            <Button colorScheme='blue' width='full' fontSize='lg' onClick={onClose}>Filter</Button>
                        </ModalBody>
                    </ModalContent>
                </Modal>
            </Container>
        </App>
    )
}