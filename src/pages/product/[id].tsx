import { BoxDeliver, ButtonOutlineIcon, DividerCustom, TextBreadcrumb, TextPrice } from "@/components/atoms";
import { IconArrowRightMore, IconAverage, IconDownList, IconLove, IconMinBuy, IconMinus, IconPlus, IconRatingLast, IconTrxSuccess } from "@/components/icons";
import { AbsoluteCenter, Box, Breadcrumb, BreadcrumbItem, BreadcrumbLink, Button, Container, Divider, Flex, FormControl, FormLabel, HStack, Image, Input, Link, Modal, ModalBody, ModalCloseButton, ModalContent, ModalHeader, ModalOverlay, SimpleGrid, Text, useDisclosure, useNumberInput } from "@chakra-ui/react";
import NextLink from "next/link";
import { useRef, useState } from "react";
import App from "../layouts/app";

export default function Show() {

    const { getInputProps, getIncrementButtonProps, getDecrementButtonProps } =
        useNumberInput({
            defaultValue: 1,
            min: 1,
            inputMode: "numeric"
        })

    const inc = getIncrementButtonProps()
    const dec = getDecrementButtonProps()
    const input = getInputProps()

    const { isOpen, onOpen, onClose } = useDisclosure()
    const finalRef = useRef(null)
    const [loadMoreTutor, setLoadMoreTutor] = useState(false)
    const [loadMoreDesc, setLoadMoreDesc] = useState(false)

    function handleLoadMoreTutor() {
        if (loadMoreTutor) {
            setLoadMoreTutor(false)
        } else {
            setLoadMoreTutor(true)
        }
    }

    function handleLoadMoreDesc() {
        if (loadMoreDesc) {
            setLoadMoreDesc(false)
        } else {
            setLoadMoreDesc(true)
        }
    }

    return (
        <App title='Detail Product'>
            <Container maxW={{ base: '8xl', xl: '7xl' }}>

                <Breadcrumb spacing='8px' separator=">" mb='3'>
                    <BreadcrumbItem>
                        <BreadcrumbLink href='/' as={NextLink}>
                            <TextBreadcrumb text="Beranda" />
                        </BreadcrumbLink>
                    </BreadcrumbItem>

                    <BreadcrumbItem>
                        <BreadcrumbLink href='#'>
                            <TextBreadcrumb text="Kategori" />
                        </BreadcrumbLink>
                    </BreadcrumbItem>

                    <BreadcrumbItem>
                        <BreadcrumbLink href='#'>
                            <TextBreadcrumb text="Mobile Legends" />
                        </BreadcrumbLink>
                    </BreadcrumbItem>

                    <BreadcrumbItem isCurrentPage>
                        <BreadcrumbLink href='#'>
                            <TextBreadcrumb isCurrentPage={true} text="Top Up 20 Diamond" />
                        </BreadcrumbLink>
                    </BreadcrumbItem>
                </Breadcrumb>

                <Flex justifyContent='space-between'>
                    <Box display='block' mx={'auto'}>
                        <Image
                            src='/images/products/mobile-legends.png'
                            alt="Mobile Legends"
                            width={700}
                            height={500}
                            objectFit='cover'
                            rounded='2xl'
                            mx={'auto'}
                        />
                        <Flex alignItems='center' gap='5' mt='5' justifyContent='center'>
                            <Box rounded='xl'>
                                <Image
                                    src='/images/products/mobile-legends.png'
                                    alt="Mobile Legends"
                                    width={230}
                                    height={150}
                                    objectFit='cover'
                                    mx={'auto'}
                                    rounded={'2xl'}
                                />
                            </Box>
                            <Box rounded='xl'>
                                <Image
                                    src='/images/products/mobile-legends.png'
                                    alt="Mobile Legends"
                                    width={230}
                                    height={150}
                                    objectFit='cover'
                                    mx={'auto'}
                                    rounded={'2xl'}
                                />
                            </Box>
                            <Box rounded='xl'>
                                <Image
                                    src='/images/products/mobile-legends.png'
                                    alt="Mobile Legends"
                                    width={230}
                                    height={150}
                                    objectFit='cover'
                                    mx={'auto'}
                                    rounded={'2xl'}
                                />
                            </Box>
                        </Flex>
                    </Box>

                    <Box rounded='xl' bg='white' border='1px' shadow='sm' px='5' py='3' width='460px' height='max-content'>
                        <Text fontSize='3xl' fontWeight='bold' mb='3'>Informasi Pesanan</Text>
                        <FormControl mb='4'>
                            <FormLabel fontSize='xl'>Metode Pembayaran Mobile Legends</FormLabel>
                            <Input type='text' variant='flushed' placeholder="Contoh : Unipin" />
                        </FormControl>
                        <FormControl mb='4'>
                            <FormLabel fontSize='xl'>Link Pembayaran Mobile Legends</FormLabel>
                            <Input type='text' variant='flushed' placeholder="Contoh : https://unipin.com" />
                        </FormControl>
                        <FormControl mb='4'>
                            <FormLabel fontSize='xl'>Catatan untuk penjual (Optional)</FormLabel>
                            <Input type='text' variant='flushed' />
                        </FormControl>
                        <Flex alignItems='center' justifyContent='space-between' mt='5'>
                            <HStack width='130px'>
                                <Button size='xl' bg='transparent' {...dec}><IconMinus /></Button>
                                <Input variant='flushed' textAlign='center' fontSize='xl' fontWeight='medium' borderBottom='1px' {...input} />
                                <Button size='xl' bg='transparent' {...inc}><IconPlus /></Button>
                            </HStack>
                            <Text rounded='lg' border='1px' borderColor='green' color='green' px='2' py='1' fontSize='sm' fontWeight='semibold'>Stok 999+</Text>
                        </Flex>
                        <Flex alignItems='center' justifyContent='space-between' mt='5'>
                            <Text fontSize='xl'>Sub Total</Text>
                            <TextPrice price='28.000' />
                        </Flex>
                        <Flex alignItems='center' justifyContent='space-between' mt='3'>
                            <Box>
                                <ButtonOutlineIcon rounded='xl' height='50px' width='190px' borderColor='primary'>
                                    <Text fontSize='2xl' color='primary' borderColor='primary'>+ Keranjang</Text>
                                </ButtonOutlineIcon>
                            </Box>
                            <Box>
                                <Link href='/checkout' as={NextLink}>
                                    <Button rounded='xl' height='50px' width='190px' fontWeight='bold' fontSize='xl' color='white' colorScheme="blue">Beli Langsung</Button>
                                </Link>
                            </Box>
                        </Flex>
                    </Box>
                </Flex>

                <Box w='57%' pt='22'>
                    <Flex alignItems='center' justifyContent='space-between'>
                        <Text fontSize='4xl' fontWeight='bold'>Mobile Legends</Text>
                        <Text fontSize='xl' fontWeight='semibold'>
                            <Flex alignItems='center'>
                                <IconLove />
                                100+
                            </Flex>
                        </Text>
                    </Flex>
                    <Flex alignItems='center' gap='20'>
                        <Text fontSize='2xl' color='gray.400' fontStyle="italic">Top Up 30 Diamonds</Text>
                        <Button variant="unstyled" onClick={onOpen}>
                            <IconDownList />
                        </Button>

                        <Modal finalFocusRef={finalRef} isOpen={isOpen} onClose={onClose} size='4xl'>
                            <ModalOverlay />
                            <ModalContent>
                                <ModalHeader>
                                    <Text fontSize='2xl' fontWeight='bold'>Pilih Banyak Top Up Diamonds</Text>
                                </ModalHeader>
                                <ModalCloseButton />
                                <ModalBody pb='12'>
                                    <SimpleGrid columns={2} spacing={5} justifyItems='center'>
                                        <Button rounded='xl' bg='white' width='340px' height='83px' border='1px' size='xl' onClick={onClose}>
                                            <Box display='block'>
                                                <Text fontSize='xl' fontWeight='semibold'>30 Diamonds</Text>
                                                <TextPrice fontSize="xl" price='25.000' />
                                            </Box>
                                        </Button>
                                        <Button rounded='xl' bg='white' width='340px' height='83px' border='1px' size='xl' onClick={onClose}>
                                            <Box display='block'>
                                                <Text fontSize='xl' fontWeight='semibold'>30 Diamonds</Text>
                                                <TextPrice fontSize="xl" price='25.000' />
                                            </Box>
                                        </Button>
                                        <Button rounded='xl' bg='white' width='340px' height='83px' border='1px' size='xl' onClick={onClose}>
                                            <Box display='block'>
                                                <Text fontSize='xl' fontWeight='semibold'>30 Diamonds</Text>
                                                <TextPrice fontSize="xl" price='25.000' />
                                            </Box>
                                        </Button>
                                        <Button rounded='xl' bg='white' width='340px' height='83px' border='1px' size='xl' onClick={onClose}>
                                            <Box display='block'>
                                                <Text fontSize='xl' fontWeight='semibold'>30 Diamonds</Text>
                                                <TextPrice fontSize="xl" price='25.000' />
                                            </Box>
                                        </Button>
                                        <Button rounded='xl' bg='white' width='340px' height='83px' border='1px' size='xl' onClick={onClose}>
                                            <Box display='block'>
                                                <Text fontSize='xl' fontWeight='semibold'>30 Diamonds</Text>
                                                <TextPrice fontSize="xl" price='25.000' />
                                            </Box>
                                        </Button>
                                        <Button rounded='xl' bg='white' width='340px' height='83px' border='1px' size='xl' onClick={onClose}>
                                            <Box display='block'>
                                                <Text fontSize='xl' fontWeight='semibold'>30 Diamonds</Text>
                                                <TextPrice fontSize="xl" price='25.000' />
                                            </Box>
                                        </Button>
                                        <Button rounded='xl' bg='white' width='340px' height='83px' border='1px' size='xl' onClick={onClose}>
                                            <Box display='block'>
                                                <Text fontSize='xl' fontWeight='semibold'>30 Diamonds</Text>
                                                <TextPrice fontSize="xl" price='25.000' />
                                            </Box>
                                        </Button>
                                        <Button rounded='xl' bg='white' width='340px' height='83px' border='1px' size='xl' onClick={onClose}>
                                            <Box display='block'>
                                                <Text fontSize='xl' fontWeight='semibold'>30 Diamonds</Text>
                                                <TextPrice fontSize="xl" price='25.000' />
                                            </Box>
                                        </Button>
                                    </SimpleGrid>
                                </ModalBody>
                            </ModalContent>
                        </Modal>
                    </Flex>

                    <Box mt='10'>
                        <TextPrice fontSize='4xl' price='28.000' />
                        <Box width='158px' mt='5'>
                            <BoxDeliver text="Pengiriman Instan" />
                        </Box>
                    </Box>

                    <Box border='1px' rounded='xl' mt='10'>
                        <Flex alignItems='center' justifyContent='space-between' px='20'>
                            <Box display='block' textAlign='center' py='2'>
                                <Flex justifyContent='center' mb='2'><IconMinBuy /></Flex>
                                <Text>Minimal Beli :</Text>
                                <Text fontWeight='bold'>1</Text>
                            </Box>
                            <Box bg='black' h='150px' w='1px'></Box>
                            <Box display='block' textAlign='center' py='2'>
                                <Flex justifyContent='center' mb='2'><IconTrxSuccess /></Flex>
                                <Text>Transaksi Sukses :</Text>
                                <Text fontWeight='bold'>8 (100%)</Text>
                            </Box>
                            <Box bg='black' h='150px' w='1px'></Box>
                            <Box display='block' textAlign='center' py='2'>
                                <Flex justifyContent='center' mb='2'><IconAverage /></Flex>
                                <Text>Rata - Rata Kirim :</Text>
                                <Text fontWeight='bold'>3 Menit</Text>
                            </Box>
                        </Flex>
                    </Box>

                    <Box rounded='xl' border='2px' borderColor='blue' px='2' py='3' bg='blue.100' color='blue.500' mt='10'>
                        <Text fontSize='md' w='600px'>Jika tidak menerima produk dalam 10 menit, kamu dapat langsung mengajukan pengembalian dana.</Text>
                    </Box>

                    <Box mb='5'>
                        <Box mt='10' maxH={loadMoreTutor ? 'full' : '170px'} overflow='hidden'>
                            <Text fontSize='2xl' fontWeight='bold' mb='5'>Cara Top up 20 Diamond Mobile Legends</Text>
                            <Text fontSize='lg' mb='5'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</Text>
                        </Box>

                        <Box textAlign='center' mt='5'>
                            <Button fontWeight='semibold' fontSize='xl' color='primary' variant='unstyled' onClick={() => handleLoadMoreTutor()}>{loadMoreTutor ? 'Tampil Sedikit' : 'Tampil Lebih Banyak'}</Button>
                        </Box>

                        <DividerCustom bg='gray.200' />
                    </Box>

                    <Box mb='5'>
                        <Box mt='10' maxH={loadMoreDesc ? 'full' : '170px'} overflow='hidden'>
                            <Text fontSize='2xl' fontWeight='bold' mb='5'>Deskripsi Produk</Text>
                            <Text fontSize='lg' mb='5'>Hi Gamers,

                                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).

                                Selamat Berbelanja sob!</Text>
                        </Box>

                        <Box textAlign='center' mt='5'>
                            <Button fontWeight='semibold' fontSize='xl' color='primary' variant='unstyled' onClick={() => handleLoadMoreDesc()}>{loadMoreDesc ? 'Tampil Sedikit' : 'Tampil Lebih Banyak'}</Button>
                        </Box>

                        <DividerCustom bg='gray.200' />
                    </Box>

                    <Box mb='16' mt='16'>
                        <Text fontSize='xl' fontWeight='semibold' mb='5'>Produk Lainnya</Text>

                        <Flex alignItems='center' gap='5' position='relative'>
                            <Box bg='white' className='shadow-product' p='5' width='250px' rounded='3xl'>
                                <Flex alignItems='center' textAlign='center' gap='4'>
                                    <Text fontSize='2xl' fontWeight='semibold'>Mobile Legends</Text>
                                </Flex>
                                <Divider orientation="horizontal" my='4' />
                                <Box mb='7'>
                                    <Text fontSize='lg' fontWeight='semibold'>20 Diamond</Text>
                                    <Text fontSize='md' fontWeight='semibold' color='gray.300'>Top Up Diamond</Text>
                                </Box>
                                <Box>
                                    <TextPrice price="28.000" />
                                    <Box width='100px'>
                                        <BoxDeliver text="Stok 999+" />
                                    </Box>
                                </Box>
                            </Box>
                            <Box bg='white' className='shadow-product' p='5' width='250px' rounded='3xl'>
                                <Flex alignItems='center' textAlign='center' gap='4'>
                                    <Text fontSize='2xl' fontWeight='semibold'>Mobile Legends</Text>
                                </Flex>
                                <Divider orientation="horizontal" my='4' />
                                <Box mb='7'>
                                    <Text fontSize='lg' fontWeight='semibold'>20 Diamond</Text>
                                    <Text fontSize='md' fontWeight='semibold' color='gray.300'>Top Up Diamond</Text>
                                </Box>
                                <Box>
                                    <TextPrice price="28.000" />
                                    <Box width='100px'>
                                        <BoxDeliver text="Stok 999+" />
                                    </Box>
                                </Box>
                            </Box>
                            <Box bg='white' className='shadow-product' p='5' width='250px' rounded='3xl'>
                                <Flex alignItems='center' textAlign='center' gap='4'>
                                    <Text fontSize='2xl' fontWeight='semibold'>Mobile Legends</Text>
                                </Flex>
                                <Divider orientation="horizontal" my='4' />
                                <Box mb='7'>
                                    <Text fontSize='lg' fontWeight='semibold'>20 Diamond</Text>
                                    <Text fontSize='md' fontWeight='semibold' color='gray.300'>Top Up Diamond</Text>
                                </Box>
                                <Box>
                                    <TextPrice price="28.000" />
                                    <Box width='100px'>
                                        <BoxDeliver text="Stok 999+" />
                                    </Box>
                                </Box>
                            </Box>

                            <AbsoluteCenter axis='vertical' right='-5'>
                                <IconArrowRightMore />
                            </AbsoluteCenter>
                        </Flex>
                    </Box>

                    <DividerCustom bg='gray.200' />

                    <Box pb='20' mt='16'>
                        <Flex alignItems='center' justifyContent='space-between' mb='5'>
                            <Flex alignItems='center' gap='3'>
                                <Text fontSize='2xl' fontWeight='semibold'>
                                    Ulasan Terakhir
                                </Text>
                                <IconRatingLast />
                                <Text color='gray.500'>5.0/5.0</Text>
                            </Flex>
                            <Text fontSize='xl' fontWeight='semibold' color='primary'>Lihat Semua</Text>
                        </Flex>

                        <Flex alignItems='center' gap='5'>
                            <Box bg='white' className='shadow-product' p='5' rounded='3xl' w='50%'>
                                <Flex alignItems='center' justifyContent='space-between'>
                                    <Text fontSize='xl' fontWeight='semibold'>A*****S</Text>
                                    <Text fontSize='sm' color='gray.500'>20 Juli 2023</Text>
                                </Flex>
                                <Flex alignItems='center' gap='2' mt='10'>
                                    <IconRatingLast />
                                    <IconRatingLast />
                                    <IconRatingLast />
                                    <IconRatingLast />
                                    <IconRatingLast />
                                </Flex>
                                <Box mt='3'>
                                    <Text>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                    </Text>
                                </Box>
                            </Box>

                            <Box bg='white' className='shadow-product' p='5' rounded='3xl' w='50%'>
                                <Flex alignItems='center' justifyContent='space-between'>
                                    <Text fontSize='xl' fontWeight='semibold'>A*****S</Text>
                                    <Text fontSize='sm' color='gray.500'>20 Juli 2023</Text>
                                </Flex>
                                <Flex alignItems='center' gap='2' mt='10'>
                                    <IconRatingLast />
                                    <IconRatingLast />
                                    <IconRatingLast />
                                    <IconRatingLast />
                                    <IconRatingLast />
                                </Flex>
                                <Box mt='3'>
                                    <Text>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                    </Text>
                                </Box>
                            </Box>
                        </Flex>
                    </Box>

                </Box>
            </Container>
        </App>
    )
}