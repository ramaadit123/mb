import { Box, Button, Checkbox, Container, Divider, Flex, FormControl, FormLabel, HStack, Image, Input, Spacer, Stack, Text, useDisclosure, useNumberInput } from "@chakra-ui/react";
import App from "../layouts/app";
import { BoxDeliver, ButtonPrimary, TextPrice } from "@/components/atoms";
import { IconArrowRight, IconLove, IconMasterCard, IconMinus, IconPlus, IconTestArrowRight } from "@/components/icons";
import IconInformationCircle from "@/components/icons/IconInformationCircle";
import { useRef, useState } from "react";
import { PaymentMethod } from "@/components/organisms";

export default function Cart() {
    const [checkedItems, setCheckedItems] = useState([false, false])

    const allChecked = checkedItems.every(Boolean)
    const isIndeterminate = checkedItems.some(Boolean) && !allChecked

    const { getInputProps, getIncrementButtonProps, getDecrementButtonProps } =
        useNumberInput({
            defaultValue: 1,
            min: 1,
            inputMode: "numeric"
        })

    const inc = getIncrementButtonProps()
    const dec = getDecrementButtonProps()
    const input = getInputProps()

    return (
        <App title='Keranjang'>
            <Container maxW='8xl' pb='20'>

                <Flex justifyContent='space-between'>
                    <Box width='full' mr='10'>
                        <Text fontSize='3xl' fontWeight='bold' mb='5'>Keranjang Belanja</Text>
                        <Flex alignItems='center' justifyContent='space-between' mb='5'>
                            <Checkbox
                                size='lg'
                                isChecked={allChecked}
                                isIndeterminate={isIndeterminate}
                                onChange={(e) => setCheckedItems([e.target.checked, e.target.checked])}
                            >
                                <Text fontSize='lg'>Pilih Semua Produk</Text>
                            </Checkbox>
                            <Text fontSize='lg' fontWeight='semibold' textAlign='end' color='primary'>Hapus</Text>
                        </Flex>
                        <Divider />
                        <Stack pl={6} mt={2} spacing={1} w='full'>
                            <Flex alignItems='start' justifyContent='space-between' py='5'>
                                <Checkbox
                                    size='lg'
                                    alignItems='start'
                                    isChecked={checkedItems[0]}
                                    onChange={(e) => setCheckedItems([e.target.checked, checkedItems[1]])}
                                >
                                    <Flex gap='5' ml='5'>
                                        <Box>
                                            <Image
                                                borderRadius='lg'
                                                boxSize='130px'
                                                w='28'
                                                h='24'
                                                objectFit='cover'
                                                src='/images/products/mobile-legends.png'
                                                alt=''
                                            />
                                        </Box>
                                        <Box>
                                            <Text py='1' fontSize='xl' fontWeight='bold'>Top up 20 Diamond</Text>
                                            <Text py='1' fontSize='sm' color='gray.400' fontStyle='italic'>Mobile Legends</Text>
                                            <Box py='1'><TextPrice fontSize='lg' price='28.000' /></Box>
                                        </Box>
                                    </Flex>
                                </Checkbox>
                                <BoxDeliver text="Pengiriman Instan" />
                            </Flex>
                            <Box pl={12}>
                                <Text fontSize='lg' fontWeight='bold'>Informasi Pesanan</Text>
                                <Flex alignItems='center' justifyContent='space-between'>
                                    <Text fontSize='lg'>Catatan untuk penjual (Optional)</Text>
                                    <Text rounded='lg' w='24' mb='3' border='1px' borderColor='green' color='green' px='2' py='1' fontSize='sm' fontWeight='semibold'>Stok 999+</Text>
                                </Flex>
                                <Flex alignItems='center' justifyContent='space-between' gap='28'>
                                    <Input type='text' variant='flushed' />
                                    <Box>
                                        <IconLove />
                                    </Box>
                                    <Box>
                                        <HStack width='130px'>
                                            <Button size='xl' bg='transparent' {...dec}><IconMinus /></Button>
                                            <Input variant='flushed' textAlign='center' fontSize='xl' fontWeight='medium' borderBottom='1px' {...input} />
                                            <Button size='xl' bg='transparent' {...inc}><IconPlus /></Button>
                                        </HStack>
                                    </Box>
                                </Flex>
                            </Box>

                            <Flex alignItems='start' justifyContent='space-between' py='5'>
                                <Checkbox
                                    size='lg'
                                    alignItems='start'
                                    isChecked={checkedItems[1]}
                                    onChange={(e) => setCheckedItems([checkedItems[0], e.target.checked])}
                                >
                                    <Flex gap='5' ml='5'>
                                        <Box>
                                            <Image
                                                borderRadius='lg'
                                                boxSize='130px'
                                                w='28'
                                                h='24'
                                                objectFit='cover'
                                                src='/images/products/mobile-legends.png'
                                                alt=''
                                            />
                                        </Box>
                                        <Box>
                                            <Text py='1' fontSize='xl' fontWeight='bold'>Top up 20 Diamond</Text>
                                            <Text py='1' fontSize='sm' color='gray.400' fontStyle='italic'>Mobile Legends</Text>
                                            <Box py='1'><TextPrice fontSize='lg' price='28.000' /></Box>
                                        </Box>
                                    </Flex>
                                </Checkbox>
                                <BoxDeliver text="Pengiriman Instan" />
                            </Flex>
                            <Box pl={12}>
                                <Text fontSize='lg' fontWeight='bold'>Informasi Pesanan</Text>
                                <Flex alignItems='center' justifyContent='space-between'>
                                    <Text fontSize='lg'>Catatan untuk penjual (Optional)</Text>
                                    <Text rounded='lg' w='24' mb='3' border='1px' borderColor='green' color='green' px='2' py='1' fontSize='sm' fontWeight='semibold'>Stok 999+</Text>
                                </Flex>
                                <Flex alignItems='center' justifyContent='space-between' gap='28'>
                                    <Input type='text' variant='flushed' />
                                    <Box>
                                        <IconLove />
                                    </Box>
                                    <Box>
                                        <HStack width='130px'>
                                            <Button size='xl' bg='transparent' {...dec}><IconMinus /></Button>
                                            <Input variant='flushed' textAlign='center' fontSize='xl' fontWeight='medium' borderBottom='1px' {...input} />
                                            <Button size='xl' bg='transparent' {...inc}><IconPlus /></Button>
                                        </HStack>
                                    </Box>
                                </Flex>
                            </Box>
                        </Stack>
                    </Box>

                    <Box rounded='xl' border='1px' borderColor='black' p='5' bg='white' height='max-content' className="shadow-checkout-box" w='2xl'>
                        <Text fontSize='2xl' fontWeight='bold'>Total Pembelian</Text>
                        <Text fontSize='xl'>( 2 Produk )</Text>
                        <Box textAlign='end'>
                            <TextPrice fontSize="2xl" price='36.000' />
                        </Box>
                        <Box mt='5'>
                            <ButtonPrimary width="full" height='12'><Text fontSize='xl'>Beli Sekarang</Text></ButtonPrimary>
                        </Box>
                    </Box>
                </Flex>

            </Container>
            <PaymentMethod />
        </App >
    )
}