import { Divider } from '@chakra-ui/react'
// import { App } from './layouts'
import { Benefit, Faq, Hero, Information, PaymentMethod, Product, Subscribe, Testimoni } from '@/components/organisms'
import App from './layouts/app'

export default function Home() {
  return (
    <App title='Homepage'>

      <section id='hero'>
        <Hero />
      </section>

      <section id='information'>
        <Information />
      </section>

      <section id='benefit'>
        <Benefit />
      </section>

      <section id='products'>
        <Product />
      </section>

      <section id='subscribe'>
        <Subscribe />
      </section>

      <section id='testimoni'>
        <Testimoni />
      </section>

      <section>
        <PaymentMethod />
      </section>

      <section id='faq'>
        <Faq />
      </section>

      <Divider mb='10' />
    </App>
  )
}
