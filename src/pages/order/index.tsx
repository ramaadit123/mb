import { ButtonPrimary, TextPrice } from "@/components/atoms";
import App from "../layouts/app";
import { Box, Button, Checkbox, Container, Divider, Flex, Image, Link, Select, Text, Input, FormControl, FormLabel } from "@chakra-ui/react";
import { IconDownList, IconInformation } from "@/components/icons";
import NextLink from "next/link";

export default function Order() {
    return (
        <App title='Konfirmasi Pesanan'>
            <Container maxW='8xl'>
                <Box py='5'>
                    <Text fontSize='2xl' fontWeight='bold'>Pesanan</Text>
                    <Text fontSize='xl'>Silahkan isi terlebih dahulu sebelum melakukan pemesanan.</Text>
                </Box>
                <Text mb='3' fontSize='2xl' fontWeight='bold'>Produk yang dipesan</Text>
                <Box mb='8' bg='white' rounded='lg' className="shadow-checkout-box" p='4'>
                    <Flex justifyContent='space-between' mb='3'>
                        <Flex gap='5'>
                            <Box>
                                <Image
                                    borderRadius='lg'
                                    boxSize='130px'
                                    w='32'
                                    h='24'
                                    objectFit='cover'
                                    src='/images/products/mobile-legends.png'
                                    alt=''
                                />
                            </Box>
                            <Box>
                                <Flex alignItems='center' gap='2' fontSize='xl'>
                                    <Text fontWeight='bold'>Mobile Legends</Text>
                                    <Text fontWeight='semibold' color='gray.400' fontStyle='italic'>- Top Up 30 Diamonds</Text>
                                </Flex>
                                <Text fontSize='xl'>Rp 25.000</Text>
                            </Box>
                        </Flex>
                        <Box>
                            <IconDownList />
                        </Box>
                    </Flex>
                    <Flex alignItems='center' justifyContent='space-between'>
                        <Text fontSize='xl' fontWeight='bold'>Total Pembayaran</Text>
                        <TextPrice fontSize='xl' price='25.000' />
                    </Flex>
                </Box>

                <Box bgColor='#FEE2E2' rounded='lg' className="shadow-checkout-box" p='6' mb='8'>
                    <Flex gap='8' alignItems='center'>
                        <IconInformation />
                        <Text fontSize='xl' fontWeight='semibold' color='red'>
                            Terkait aturan yang berlaku dari Youtube, sebelum melakukan pendaftaran pastikan akun Youtube kamu belum pernah berpindah family selama 12 bulan terakhir.
                        </Text>
                    </Flex>
                </Box>

                <Box pb='20'>
                    <Text fontSize='2xl' fontWeight='bold' mb='2'>Informasi Pengguna</Text>
                    <FormControl mb='2'>
                        <FormLabel fontSize='lg'>Nama Lengkap</FormLabel>
                        <Input type='text' size='lg' border='1px' placeholder='Masukkan Nama' />
                    </FormControl>
                    <FormControl mb='2'>
                        <FormLabel fontSize='lg'>Email</FormLabel>
                        <Input type='text' size='lg' border='1px' placeholder='Masukkan Email' />
                    </FormControl>

                    <FormLabel fontSize='lg'>Nomor WhatsApp (Pastikan nomor sudah benar dan aktif)</FormLabel>
                    <Flex alignItems='center' gap='2'>
                        <FormControl mb='2' w='32'>
                            <Select size='lg' border='1px'>
                                <option value='option1'>+62</option>
                            </Select>
                        </FormControl>
                        <FormControl mb='2'>
                            <Input type='text' size='lg' border='1px' placeholder='Masukkan Email' />
                        </FormControl>
                    </Flex>
                    <Box mt='3'>
                        <Checkbox size='lg'>
                            Menyetujui <Link href="#" as={NextLink} textDecoration='underline'>aturan</Link> yang dibuat oleh Seakun.id
                        </Checkbox>
                    </Box>
                    <Box my='3'>
                        <ButtonPrimary w="full">Konfirmasi Pesanan</ButtonPrimary>
                    </Box>
                </Box>
            </Container>
        </App>
    )
}
