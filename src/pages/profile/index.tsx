import { AbsoluteCenter, Box, Button, Container, Divider, Flex, Image, SimpleGrid, Text } from "@chakra-ui/react";
import App from "../layouts/app";
import { IconArrowRight, IconCoin, IconCoupon, IconEdit, IconFacebookColor, IconInstagramColor, IconLogout, IconTwitterColor, IconYoutubeColor } from "@/components/icons";

export default function index() {
    return (
        <App title="Profile">
            <Box bg='abu-abu'>
                <Box bg='primary' w='full' h='64' justifyItems='center' mt='-5' p='10'>
                    <Flex alignItems='center' justifyContent='center' gap='10'>
                        <Box>
                            <Image
                                borderRadius='full'
                                boxSize='100px'
                                src='/images/profiles/profile-default.png'
                                alt=''
                            />
                        </Box>
                        <Box>
                            <Flex alignItems='center' gap='16' mb='5'>
                                <Text fontSize='3xl' fontWeight='semibold' color='white'>Anonymouse</Text>
                                <IconEdit />
                            </Flex>
                            <Flex alignItems='center' gap='5'>
                                <Box bg='white' rounded='full' p='2'>
                                    <Flex alignItems='center' gap='2'>
                                        <IconCoin />
                                        <Text>0 Coin</Text>
                                    </Flex>
                                </Box>
                                <Box bg='white' rounded='full' p='2'>
                                    <Flex alignItems='center' gap='2'>
                                        <IconCoupon />
                                        <Text>0 Coupon</Text>
                                    </Flex>
                                </Box>
                            </Flex>
                        </Box>
                    </Flex>
                </Box>

                <Container maxW='8xl'>
                    <Box bg='white' w='full' position='relative' px='10' pt='32' pb='10' mb='5'>
                        <AbsoluteCenter axis='horizontal' top='-20'>
                            <Box mx='auto' bg='white' justifyItems='center' className='shadow-checkout-box' rounded='xl' width='650px' height='160px'>
                                <SimpleGrid columns={3}>
                                    <Box py='5' height='160px' textAlign='center' borderRight='1px' borderColor='gray.400'>
                                        <Flex justifyContent='center'>
                                            <Image
                                                src='/images/dana.png'
                                                width='50px'
                                                height='50px'
                                                alt=''
                                                objectFit='cover'
                                            />
                                        </Flex>
                                        <Text fontSize='xl'>Dana</Text>
                                        <Text color='primary' fontWeight='bold'>Hubungkan</Text>
                                    </Box>
                                    <Box py='5' height='160px' textAlign='center'>
                                        <Flex justifyContent='center'>
                                            <Image
                                                src='/images/ovo.png'
                                                width='50px'
                                                height='50px'
                                                alt=''
                                                objectFit='cover'
                                            />
                                        </Flex>
                                        <Text fontSize='xl'>OVO</Text>
                                        <Text color='primary' fontWeight='bold'>Hubungkan</Text>
                                    </Box>
                                    <Box py='5' height='160px' textAlign='center' borderLeft='1px' borderColor='gray.400'>
                                        <Flex justifyContent='center'>
                                            <Image
                                                src='/images/dompet.png'
                                                width='50px'
                                                height='50px'
                                                alt=''
                                                objectFit='cover'
                                            />
                                        </Flex>
                                        <Text fontSize='xl'>Dompetku</Text>
                                        <Text color='primary' fontWeight='bold'>Rp0</Text>
                                    </Box>
                                </SimpleGrid>
                            </Box>
                        </AbsoluteCenter>

                        <Text fontSize='2xl' fontWeight='bold'>Riwayat Pembelian</Text>
                        <Flex alignItems='center' justifyContent='space-between' mt='3'>
                            <Text fontSize='xl'>Lihat Riwayat Pembelian</Text>
                            <IconArrowRight />
                        </Flex>
                    </Box>

                    <Box bg='white' p='8' px='10' mb='5'>
                        <Text fontSize='2xl' fontWeight='bold'>Bantuan</Text>
                        <Box my='3'>
                            <Text fontSize='xl' fontWeight='semibold'>Pusat Bantuan</Text>
                            <Flex justifyContent='space-between' alignItems='center'>
                                <Text fontSize='lg'>Temukan pertanyaan kamu di bagian tanya jawab sebelum kamu menghubungi kami.</Text>
                                <IconArrowRight />
                            </Flex>
                        </Box>
                        <Divider borderColor='gray.400' />
                        <Box my='3'>
                            <Text fontSize='xl' fontWeight='semibold'>Kendala Pesanan</Text>
                            <Flex justifyContent='space-between' alignItems='center'>
                                <Text fontSize='lg'>Temukan solusi dengan cepat bersama penjual tanpa perlu menghubungi CS maubang terlebih dahulu.</Text>
                                <IconArrowRight />
                            </Flex>
                        </Box>
                    </Box>

                    <Box bg='white' p='8' px='10' mb='5'>
                        <Text fontSize='2xl' fontWeight='bold'>Tentang Kami</Text>
                        <Box my='3'>
                            <Flex justifyContent='space-between' alignItems='center'>
                                <Text fontSize='xl' fontWeight='semibold'>Tentang Maubang.</Text>
                                <IconArrowRight />
                            </Flex>
                        </Box>
                        <Divider borderColor='gray.400' />
                        <Box my='3'>
                            <Flex justifyContent='space-between' alignItems='center'>
                                <Text fontSize='xl' fontWeight='semibold'>Kebijakan Privasi.</Text>
                                <IconArrowRight />
                            </Flex>
                        </Box>
                        <Divider borderColor='gray.400' />
                        <Box my='3'>
                            <Flex justifyContent='space-between' alignItems='center'>
                                <Text fontSize='xl' fontWeight='semibold'>Karir.</Text>
                                <IconArrowRight />
                            </Flex>
                        </Box>
                    </Box>

                    <Box bg='white' p='8' px='10' pb='5'>
                        <Button variant='outline' size='lg' w='full' color='primary' py='8' border='1px' borderColor='primary'>
                            <Flex alignItems='center' gap='5'>
                                <IconLogout />
                                <Text fontSize='2xl' fontWeight='bold'>LOGOUT</Text>
                            </Flex>
                        </Button>

                        <Box mt='8' textAlign='center'>
                            <Text>Ikuti Maubang</Text>
                            <Box mt='3'>
                                <Flex alignItems='center' justifyContent='center' gap='3'>
                                    <IconFacebookColor />
                                    <IconInstagramColor />
                                    <IconYoutubeColor />
                                    <IconTwitterColor />
                                </Flex>
                            </Box>
                        </Box>
                    </Box>
                </Container>
            </Box>
        </App>
    )
}
