import { AbsoluteCenter, Box, Container, Flex, Image, Input, InputGroup, InputRightElement, Text } from "@chakra-ui/react";
import App from "../layouts/app";
import { IconEdit, IconVerified } from "@/components/icons";

export default function Edit() {
    return (
        <App title='Edit Profile'>
            <Box bg='abu-abu' mt='-5'>
                <Container maxW='8xl'>
                    <Box bg='white' p='8'>
                        <Box position='relative' w='max-content'>
                            <Image
                                borderRadius='full'
                                boxSize='200px'
                                src='/images/profiles/profile-default.png'
                                alt=''
                            />
                            <AbsoluteCenter axis='horizontal' left='40' bottom='0'>
                                <Box bg='white' p='2' rounded='full' className='shadow-checkout-box'>
                                    <IconEdit />
                                </Box>
                            </AbsoluteCenter>
                        </Box>

                        <Text mt='7' mb='4'>Pastikan gambar berformat .JPG / .PNG / .GIF</Text>

                        <Box mb='5' pr='10'>
                            <Text fontSize='xl' mb='2'>Nama</Text>
                            <Text fontSize='xl' ml='5'>Anonymouse</Text>
                        </Box>
                        <Box mb='5' pr='10'>
                            <Text fontSize='xl' mb='2'>Email</Text>
                            <Flex justifyContent='space-between' alignItems='center'>
                                <Text fontSize='xl' ml='5'>anonymouse@gmail.com</Text>
                                <IconVerified />
                            </Flex>
                        </Box>
                        <Box mb='5' pr='10'>
                            <Text fontSize='xl' mb='2'>No. Handphone</Text>
                            <Input ml='5' variant='flushed' borderBottom='1px' borderColor='black' value='+62' />
                        </Box>
                        <Box mb='5' pr='10'>
                            <Text fontSize='xl' mb='2'>Jenis Kelamin</Text>
                            <Input ml='5' variant='flushed' borderBottom='1px' borderColor='black' value='' />
                        </Box>
                        <Box mb='5' pr='10'>
                            <Text fontSize='xl' mb='2'>Tanggal Lahir</Text>
                            {/* <InputGroup>
                                <Input ml='5' type="date" variant='flushed' borderBottom='1px' borderColor='black' />
                                <InputRightElement>
                                    <IconEdit />
                                </InputRightElement>
                            </InputGroup> */}
                            <Input ml='5' type="date" variant='flushed' borderBottom='1px' borderColor='black' value='' />
                        </Box>
                        <Box pb='8'>
                            <Text>Data yang kamu masukan tidak akan terlihat di publil.</Text>
                        </Box>
                    </Box>
                </Container>
            </Box>
        </App>
    )
}
