import { ChakraProvider } from '@chakra-ui/react'
import type { AppProps } from 'next/app'
import { DM_Sans, Rubik } from 'next/font/google'
import { theme } from '../../theme'
import '../styles/globals.css'
import NextNProgressProps from 'nextjs-progressbar'

const dm_sans = DM_Sans({ subsets: ['latin'] })

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <style jsx global>
        {`
      :root {
        --font-dm_sans : ${dm_sans.style.fontFamily}
      }
    `}
      </style>
      <ChakraProvider theme={theme}>
        <NextNProgressProps />
        <Component {...pageProps} />
      </ChakraProvider>
    </>
  )
}
