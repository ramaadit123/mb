export default function IconTestArrowRight() {
    return (
        <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="30" cy="30" r="30" transform="rotate(-180 30 30)" fill="#307FE2" />
            <path d="M26.6364 50L47 29.5L26.6364 9L19 16.6875L31.7273 29.5L19 42.3125L26.6364 50Z" fill="white" />
        </svg>
    )
}
