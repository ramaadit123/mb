export default function IconAccordingUp() {
    return (
        <svg width="31" height="18" viewBox="0 0 31 18" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M28 15.5L15.5 3L3 15.5" stroke="#D9D9D9" strokeWidth="5" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
    )
}
