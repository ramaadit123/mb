export default function IconDropDown() {
    return (
        <svg width="30" height="20" viewBox="0 0 30 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2.01858 0H27.9814C29.7775 0 30.6756 2.54966 29.4042 4.04388L16.4278 19.3063C15.6407 20.2312 14.3593 20.2312 13.5722 19.3063L0.595824 4.04388C-0.675578 2.54966 0.222476 0 2.01858 0Z" fill="#D9D9D9" />
        </svg>
    )
}
