export default function IconTestArrrowLeft() {
    return (
        <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="30" cy="30" r="30" fill="#307FE2" />
            <path d="M33.3636 10L13 30.5L33.3636 51L41 43.3125L28.2727 30.5L41 17.6875L33.3636 10Z" fill="white" />
        </svg>
    )
}
