import IconArrowRight from "./IconArrowRight"
import IconAverage from "./IconAverage"
import IconBell from "./IconBell"
import IconCarBox from "./IconCarBox"
import IconCategory from "./IconCategory"
import IconChart from "./IconChart"
import IconCheck from "./IconCheck"
import IconClock from "./IconClock"
import IconCoin from "./IconCoin"
import IconCoupon from "./IconCoupon"
import IconDiscount from "./IconDiscount"
import IconDropDown from "./IconDropDown"
import IconEdit from "./IconEdit"
import IconEmail from "./IconEmail"
import IconEnvelop from "./IconEnvelop"
import IconFacebookColor from "./IconFacebookColor"
import IconGoogle from "./IconGoogle"
import IconHeadphone from "./IconHeadphone"
import IconInformationCirle from "./IconInformationCircle"
import IconInstagram from "./IconInstagram"
import IconInstagramColor from "./IconInstagramColor"
import IconKey from "./IconKey"
import IconLinkedin from "./IconLinkedin"
import IconLogout from "./IconLogout"
import IconLove from "./IconLove"
import IconMinBuy from "./IconMinBuy"
import IconMoney from "./IconMoney"
import IconNotification from "./IconNotification"
import IconPhone from "./IconPhone"
import IconRating from "./IconRating"
import IconSearch from "./IconSearch"
import IconService from "./IconServce"
import IconStar from "./IconStar"
import IconTransaction from "./IconTransaction"
import IconTrxSuccess from "./IconTrxSuccess"
import IconTwitter from "./IconTwitter"
import IconTwitterColor from "./IconTwitterColor"
import IconUsers from "./IconUsers"
import IconVerified from "./IconVerified"
import IconYoutubeColor from "./IconYoutubeColor"
import IconTestArrowRight from "./IconTestArrowRight"
import IconTestArrowLeft from "./IconTestArrrowLeft"
import IconAccordingUp from "./IconAccordingUp"
import IconAccordingDown from "./IconAccordingDown"
import IconPlus from "./IconPlus"
import IconMinus from "./IconMinus"
import IconDownList from "./IconDownList"
import IconArrowRightMore from "./IconArrowRightMore"
import IconRatingLast from "./IconRatingLast"
import IconSortUp from "./IconSortUp"
import IconSortDown from "./IconSortDown"
import IconSort from "./IconSort"
import IconFilter from "./IconFilter"
import IconMasterCard from "./IconMasterCard"
import IconTruckBlue from "./IconTruckBlue"
import IconInformation from "./IconInformation"

export {
    IconMasterCard,
    IconInformation,
    IconTruckBlue,
    IconArrowRight,
    IconAverage,
    IconBell,
    IconCarBox,
    IconCategory,
    IconChart,
    IconCheck,
    IconClock,
    IconCoin,
    IconCoupon,
    IconDiscount,
    IconEdit,
    IconEmail,
    IconEnvelop,
    IconFacebookColor,
    IconGoogle,
    IconHeadphone,
    IconInformationCirle,
    IconInstagram,
    IconInstagramColor,
    IconKey,
    IconLinkedin,
    IconLogout,
    IconLove,
    IconMinBuy,
    IconMoney,
    IconNotification,
    IconPhone,
    IconRating,
    IconSearch,
    IconService,
    IconStar,
    IconTransaction,
    IconTrxSuccess,
    IconTwitter,
    IconTwitterColor,
    IconUsers,
    IconVerified,
    IconYoutubeColor,
    IconDropDown,
    IconTestArrowRight,
    IconTestArrowLeft,
    IconAccordingDown,
    IconAccordingUp,
    IconPlus,
    IconMinus,
    IconDownList,
    IconArrowRightMore,
    IconRatingLast,
    IconSortUp,
    IconSortDown,
    IconSort,
    IconFilter,
}