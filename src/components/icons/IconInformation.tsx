export default function IconInformation() {
    return (
        <svg width="36" height="34" viewBox="0 0 36 34" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M17.1203 1.73072C17.4983 1.03137 18.5017 1.03137 18.8797 1.73072L34.5778 30.7745C34.938 31.4408 34.4555 32.25 33.6981 32.25H2.30189C1.54451 32.25 1.06204 31.4408 1.42216 30.7745L17.1203 1.73072Z" stroke="#F04E8C" stroke-width="2" />
        </svg>
    )
}
