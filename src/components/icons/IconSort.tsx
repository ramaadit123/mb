export default function IconSort() {
    return (
        <svg version="1.0" xmlns="http://www.w3.org/2000/svg" strokeWidth="2"
            height="28" viewBox="0 0 52.000000 84.000000"
            preserveAspectRatio="xMidYMid meet">

            <g transform="translate(0.000000,84.000000) scale(0.100000,-0.100000)"
                fill="#307FE2" stroke="#307FE2">
                <path d="M144 653 l-115 -106 25 -25 25 -25 95 88 96 87 96 -87 95 -88 25 25
25 25 -115 106 c-64 59 -120 107 -126 107 -6 0 -62 -48 -126 -107z"/>
                <path d="M51 276 l-22 -23 115 -107 c64 -58 120 -106 126 -106 6 0 62 48 126
107 l115 106 -25 25 -25 25 -95 -88 -96 -87 -94 86 c-52 47 -96 86 -98 86 -3
0 -15 -11 -27 -24z"/>
            </g>
        </svg>
    )
}