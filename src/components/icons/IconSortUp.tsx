export default function IconSortUp() {
    return (
        <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M12 7L6.5 2L1 7" stroke="#307FE2" stroke-width="2" />
        </svg>
    )
}