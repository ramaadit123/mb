export default function IconArrowRightMore() {
    return (
        <svg width="68" height="68" viewBox="0 0 68 68" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g filter="url(#filter0_d_73_1370)">
                <circle cx="34" cy="30" r="30" transform="rotate(-180 34 30)" fill="white" />
            </g>
            <path d="M33.5876 45L48.4878 30L33.5876 15L28 20.625L37.3126 30L28 39.375L33.5876 45Z" fill="black" fill-opacity="0.35" />
            <defs>
                <filter id="filter0_d_73_1370" x="0" y="0" width="68" height="68" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix" />
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                    <feOffset dy="4" />
                    <feGaussianBlur stdDeviation="2" />
                    <feComposite in2="hardAlpha" operator="out" />
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_73_1370" />
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_73_1370" result="shape" />
                </filter>
            </defs>
        </svg>
    )
}
