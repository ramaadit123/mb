export default function IconAccordingDown() {
    return (
        <svg width="31" height="18" viewBox="0 0 31 18" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M3 3L15.5 15.5L28 3" stroke="#D9D9D9" strokeWidth="5" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
    )
}
