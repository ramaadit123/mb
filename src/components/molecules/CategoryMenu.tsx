import { CategoryMenuProps } from "@/types";
import { Flex, Link, Text } from "@chakra-ui/react";
import NextLink from "next/link";

export default function CategoryMenu(props: CategoryMenuProps) {
    const { href = '#', name } = props;
    return (
        <Link href={href} as={NextLink} _hover={{ textDecoration: 'none' }}>
            <Flex alignItems='center' justifyContent='space-between'>
                <Text fontSize='2xl' fontWeight='semibold'>{name}</Text>
                <Text fontSize='4xl' fontWeight='semibold' color='gray.300'>{'>'}</Text>
            </Flex>
        </Link>
    )
}