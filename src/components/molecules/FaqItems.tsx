import { Accordion, AccordionButton, AccordionItem, AccordionPanel, Box } from "@chakra-ui/react";
import { IconAccordingDown, IconAccordingUp } from "../icons";
import { FaqItemsProps } from "@/types";

export default function FaqItems(props: FaqItemsProps) {
    const { q, a } = props

    return (
        <Accordion allowToggle>
            <AccordionItem className='shadow-product' rounded='lg'>
                {({ isExpanded }) => (
                    <>
                        <h2>
                            <AccordionButton>
                                <Box as="span" flex='1' textAlign='left' fontSize='xl' fontWeight='semibold'>
                                    {q}
                                </Box>
                                {isExpanded ? (
                                    <IconAccordingUp />
                                ) : (
                                    <IconAccordingDown />
                                )}
                            </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4} bg='primary' color='white' fontSize='lg' fontWeight='semibold' roundedBottom='md'>
                            {a}
                        </AccordionPanel>
                    </>
                )}
            </AccordionItem>
        </Accordion>
    )
}
