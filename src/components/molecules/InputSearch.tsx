import { Input, InputGroup, InputLeftElement } from "@chakra-ui/react";
import { IconSearch } from "../icons";

export interface types {
    type: string
    text: string
    width?: any
    height?: any
}

export default function InputSearch(props: types) {
    const { type = 'text', text, width, height } = props

    return (
        <InputGroup rounded='xl' className='shadow-information' width={width}>
            <InputLeftElement pointerEvents='none' height={height}>
                <IconSearch />
            </InputLeftElement>
            <Input type={type} placeholder={text} height={height} />
        </InputGroup>
    )
}
