import { Select } from "@chakra-ui/react";
import { IconDropDown } from "../icons";

export interface types {
    text: string
    children: any
    height?: any
    width?: any
}

export default function InputSelect(props: types) {
    const { text, children, height, width } = props

    return (
        <Select icon={<IconDropDown />} placeholder={text} color='gray.400' className='shadow-information' width={width} height={height}>
            {children}
        </Select>
    )
}
