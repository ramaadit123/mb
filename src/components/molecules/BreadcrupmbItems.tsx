import { Breadcrumb, BreadcrumbItem, BreadcrumbLink } from "@chakra-ui/react";
import { TextBreadcrumb } from "../atoms";
import NextLink from "next/link"

export default function BreadcrupmbItems() {
    return (
        <Breadcrumb spacing='8px' separator=">" mb='3'>
            <BreadcrumbItem>
                <BreadcrumbLink href='#'>
                    <TextBreadcrumb text="Beranda" />
                </BreadcrumbLink>
            </BreadcrumbItem>

            <BreadcrumbItem>
                <BreadcrumbLink href='#'>
                    <TextBreadcrumb text="Kategori" />
                </BreadcrumbLink>
            </BreadcrumbItem>

            <BreadcrumbItem>
                <BreadcrumbLink href='#'>
                    <TextBreadcrumb text="Mobile Legends" />
                </BreadcrumbLink>
            </BreadcrumbItem>

            <BreadcrumbItem isCurrentPage>
                <BreadcrumbLink href='#'>
                    <TextBreadcrumb isCurrentPage={true} text="Top Up 20 Diamond" />
                </BreadcrumbLink>
            </BreadcrumbItem>
        </Breadcrumb>
    )
}