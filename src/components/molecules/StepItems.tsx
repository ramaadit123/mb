import { Box, Flex } from "@chakra-ui/react";
import Image from "next/image";
import { StepBadge, StepText } from "../atoms";
import { StepItemsProps } from "@/types";

export default function StepItems(props: StepItemsProps) {
    const { image, step, text } = props

    return (
        <Flex alignItems='center' gap='5' justifyContent='center'>
            <Box display='block'>
                <Image
                    src={image}
                    alt=''
                    width='191'
                    height='191'
                />
                <Flex alignItems='center' justifyContent='center' gap='2' py='3'>
                    <StepBadge step={step} />
                    <StepText text={text} />
                </Flex>
            </Box>
        </Flex>
    )
}