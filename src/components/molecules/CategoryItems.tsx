import { CategoryItemsProps } from "@/types";
import { Flex, Link, Text } from "@chakra-ui/react";
import Image from "next/image";

export default function CategoryItems(props: CategoryItemsProps) {
    const { image, name, href } = props

    return (
        <Flex alignItems='center' gap='3'>
            <Image
                src={image}
                alt=""
                width='50'
                height='50'
                objectFit="cover"
            />
            <Link href={href} fontSize='xl' fontWeight='semibold'>{name}</Link>
        </Flex>
    )
}