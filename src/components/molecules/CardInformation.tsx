import { AbsoluteCenter, Box, Text } from "@chakra-ui/react";

export interface types {
    icon: any
    title: string
    subtitle: string
}

export default function CardInformation(props: types) {
    const { icon, title, subtitle } = props

    return (
        <Box className='shadow-information' bgColor='white' rounded='xl' width={{ base: '340px', md: '235px', xl: '235px' }} height='116px' px='7' py='5' position='relative'>
            <Text fontSize='3xl' fontWeight='bold' color='primary'>{title}</Text>
            <Text fontSize='lg'>{subtitle}</Text>

            <AbsoluteCenter axis='horizontal' top='-5' left='0'>
                {icon}
            </AbsoluteCenter>
        </Box>
    )
}
