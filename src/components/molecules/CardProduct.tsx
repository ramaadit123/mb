import { AbsoluteCenter, Box, Divider, Flex, LinkBox, Text } from "@chakra-ui/react";
import Image from "next/image";
import { BoxDeliver, TextPrice } from "../atoms";
import NextLink from "next/link";

export interface types {
    image: string,
    category: string
    title: string
    description: string
    price: string
    deliverType: string
    sold: number
    href: string
    width?: any
}

export default function CardProduct(props: types) {
    const { image, category, title, description, price, deliverType, sold, href, width = '286px' } = props

    return (
        <LinkBox href={href} as={NextLink} bg='white' className='shadow-product' p='5' width={width} height='354px' position='relative' rounded='lg' overflow='scroll'>
            <Flex alignItems='center' gap='4'>
                <Image
                    src={image}
                    alt={category}
                    width='80'
                    height='50'
                />
                <Text fontSize='2xl' fontWeight='semibold'>{category}</Text>
            </Flex>
            <Divider orientation="horizontal" my='4' />
            <Box mb='7'>
                <Text fontSize='lg' fontWeight='semibold'>{title}</Text>
                <Text fontSize='md' fontWeight='semibold' color='gray.300'>{description}</Text>
            </Box>
            <Box>
                <TextPrice price={price} />
                <Box width='155px'>
                    <BoxDeliver text={deliverType} />
                </Box>
            </Box>
            <AbsoluteCenter axis='horizontal' bottom='3' left='12'>
                <Text fontSize='sm' color='gray.400'>{sold} Terjual</Text>
            </AbsoluteCenter>
        </LinkBox>
    )
}
