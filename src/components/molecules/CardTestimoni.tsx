import { Box, Flex, Text, Divider } from "@chakra-ui/react";
import Image from "next/image"
import { IconStar } from "../icons";
import { CardTestimoniProps } from "@/types";

export default function CardTestimoni(props: CardTestimoniProps) {
    const { image, name, text, star } = props

    return (
        <Box className='shadow-product' w={{ base: 'full', md: '300px', xl: '300px' }} h='355px' p='4' rounded='xl'>
            <Flex alignItems='center' gap='3'>
                <Box rounded='full'>
                    <Image
                        src={image}
                        alt=''
                        width='52'
                        height='60'
                        style={{ objectFit: 'cover' }}
                    />
                </Box>
                <Box display='block'>
                    <Text fontSize='lg' fontWeight='semibold'>{name}</Text>
                    <Flex alignItems='center'>
                        <IconStar />
                        <IconStar />
                        <IconStar />
                        <IconStar />
                        <IconStar />
                    </Flex>
                </Box>
            </Flex>
            <Divider orientation="horizontal" py='2' />
            <Text textAlign='center' mt='2'>
                {text}
            </Text>
        </Box>
    )
}