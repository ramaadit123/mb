import { Text } from "@chakra-ui/react";

export interface types {
    text: string,
    fontSize?: any,
    w?: any
}

export default function TextSubtitle(props: types) {
    const { text, fontSize = '2xl', w } = props

    return (
        <Text w={w} fontSize={fontSize} mx='auto' mt='6' width={{ base: '', md: '4xl', xl: '4xl' }}>{text}</Text>
    )
}
