import { StepTextProps } from "@/types";
import { Text } from "@chakra-ui/react";

export default function StepText(props: StepTextProps) {
    const { text } = props
    return (
        <Text fontSize='lg' color='primary' fontWeight='semibold'>{text}</Text>
    )
}