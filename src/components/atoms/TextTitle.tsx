import { TextTitleProps } from "@/types";
import { Text } from "@chakra-ui/react";

export default function TextTitle(props: TextTitleProps) {
    const { text, color = 'primary', fontSize = '4xl', mt = '16' } = props

    return (
        <Text fontSize={fontSize} mt={mt} fontWeight='bold' color={color}>{text}</Text>
    )
}
