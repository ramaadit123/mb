import { ButtonPrimaryProps } from "@/types";
import { Button } from "@chakra-ui/react";

export default function ButtonPrimary(props: ButtonPrimaryProps) {
    const { width, size, w, h, height, px, py, children } = props

    return (
        <Button color='white' colorScheme="blue" width={width} size={size} w={w} h={h} height={height} px={px} py={py}>{children}</Button>
    )
}
