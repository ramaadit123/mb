import { Box, Text } from "@chakra-ui/react";

export interface types {
    text: string
    bg?: string
}

export default function BoxDeliver(props: types) {
    const { text, bg = 'light-green' } = props

    return (
        <Box bg={bg} p='2' rounded='md'>
            <Text fontSize='md' fontWeight='semibold' color='green'>{text}</Text>
        </Box>
    )
}
