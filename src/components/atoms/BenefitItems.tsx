import { Flex, Text } from "@chakra-ui/react";

export interface types {
    text: string,
    icon: any
}

export default function BenefitItems(props: types) {
    const { text, icon } = props

    return (
        <Flex alignItems='center' gap='3' mb='4'>
            {icon}
            <Text fontWeight='semibold' fontSize='xl'>{text}</Text>
        </Flex>
    )
}
