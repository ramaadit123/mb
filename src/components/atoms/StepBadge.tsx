import { StepBadgeProps } from "@/types";
import { Text } from "@chakra-ui/react";

export default function StepBadge(props: StepBadgeProps) {
    const { step } = props;

    return (
        <Text fontSize='sm' color='white' bg='primary' px='3' py='1' rounded='full'>{step}</Text>
    )
}
