import { Text } from "@chakra-ui/react";

export interface types {
    text: string
    isCurrentPage?: boolean
}

export default function TextBreadcrumb(props: types) {
    const { text, isCurrentPage = false } = props
    return (
        <Text fontWeight='semibold' color={isCurrentPage ? 'black' : 'primary'} fontSize='lg'>{text}</Text>
    )
}