import { Box } from "@chakra-ui/react";

export interface types {
    bg?: string
}

export default function DividerCustom(props: types) {
    const { bg = 'gray.900' } = props

    return (
        <Box w='full' h='2px' bg={bg}></Box>
    )
}
