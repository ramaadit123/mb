import { ButtonOulineIconProps } from "@/types";
import { Button } from "@chakra-ui/react";

export default function ButtonOutlineIcon(props: ButtonOulineIconProps) {
    const { icon, children, onClick, w = 'lg', size = 'xl', width, h, height, rounded, borderColor, border = '1px' } = props
    return (
        <Button leftIcon={icon} variant='outline' w={w} width={width} size={size} rounded={rounded} h={h} height={height} py='2' border={border} borderColor={borderColor} onClick={onClick}>{children}</Button>
    )
}
