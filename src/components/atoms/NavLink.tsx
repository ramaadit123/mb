import { NavLinkProps } from "@/types";
import { Link } from "@chakra-ui/react";
import NextLink from "next/link";

export default function NavLink(props: NavLinkProps) {
    const { href = '#', children, fontSize, color = 'black', fontWeight } = props

    return (
        <Link href={href} as={NextLink} _hover={{ textDecorationLine: 'none' }} fontSize={fontSize} color={color} fontWeight={fontWeight}>{children}</Link>
    )
}