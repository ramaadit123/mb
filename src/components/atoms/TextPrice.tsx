import { Text } from "@chakra-ui/react";

export interface types {
    fontSize?: string
    price: string
}

export default function TextPrice(props: types) {
    const { fontSize = '3xl', price } = props
    return (
        <Text fontSize={fontSize} color='primary' fontWeight='bold'>Rp{price}</Text>
    )
}
