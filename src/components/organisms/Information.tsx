import { Container, Flex } from "@chakra-ui/react";
import { IconRating, IconService, IconTransaction, IconUsers } from "../icons";
import CardInformation from "../molecules/CardInformation";

export default function Information() {
    return (
        <Container maxW='8xl' pb={{ base: '8', md: '20', xl: '20' }}>
            <Flex alignItems='center' gap={{ base: '8', md: '20', xl: '20' }} justifyContent='center' wrap={{ base: 'wrap', md: 'nowrap', xl: 'nowrap' }}>
                <CardInformation icon={<IconUsers />} title="100.000 +" subtitle="Pengguna" />
                <CardInformation icon={<IconTransaction />} title="210.000" subtitle="Transaksi" />
                <CardInformation icon={<IconService />} title="29" subtitle="Layanan" />
                <CardInformation icon={<IconRating />} title="9/10" subtitle="Tingkat Kepuasan" />
            </Flex>
        </Container>
    )
}
