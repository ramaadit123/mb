import Benefit from "./Benefit"
import Hero from "./Hero"
import Information from "./Information"
import Product from "./Product"
import Subscribe from "./Subscribe"
import Testimoni from "./Testimoni"
import PaymentMethod from "./PaymentMethod"
import Faq from "./Faq"

export {
    Benefit,
    Hero,
    Information,
    Product,
    Subscribe,
    Testimoni,
    PaymentMethod,
    Faq,
}