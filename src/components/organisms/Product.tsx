import { Box, Container, Flex, Grid, Link } from "@chakra-ui/react";
import { ButtonPrimary, TextTitle } from "../atoms";
import CardProduct from "../molecules/CardProduct";
import InputSearch from "../molecules/InputSearch";
import InputSelect from "../molecules/InputSelect";
import NextLink from "next/link";

export default function Product() {
    return (
        <Container maxW={{ base: '8xl', xl: '7xl' }} pb={{ base: '10', md: '20', xl: '20' }}>
            <TextTitle fontSize={{ base: '3xl', md: '4xl', xl: '4xl' }} text='Berlangganan Product Digital' />
            <Flex alignItems='center' justifyContent='space-between' mt='5' mb='10' gap={{ base: '5', md: '0', xl: '0' }} wrap={{ base: 'wrap', md: 'nowrap', xl: 'nowrap' }}>
                {/* <InputSearch text="Cari Product" height="12" width={{ base: 'full', md: 'xl', xl: 'xl' }} /> */}
                <InputSelect text='Tipe Product' height="12" width={{ base: 'full', md: '3xs', xl: '3xs' }}>
                    <option value="test">Test</option>
                </InputSelect>
            </Flex>
            <Grid templateColumns={{ base: 'repeat(1, 1fr)', md: 'repeat(4, 1fr)', xl: 'repeat(4, 1fr)' }} gap={6} mb={{ base: '10', md: '24', xl: '24' }} justifyItems='center'>
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/Youtube.png" category="Youtube" title="User Premium 1 Bulan" description="User Premium" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/Youtube.png" category="Youtube" title="User Premium 1 Bulan" description="User Premium" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/Youtube.png" category="Youtube" title="User Premium 1 Bulan" description="User Premium" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/Youtube.png" category="Youtube" title="User Premium 1 Bulan" description="User Premium" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/game.png" category="Mobile Legends" title="20 Diamond" description="Top Up Diamond" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/game.png" category="Mobile Legends" title="20 Diamond" description="Top Up Diamond" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/game.png" category="Mobile Legends" title="20 Diamond" description="Top Up Diamond" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/game.png" category="Mobile Legends" title="20 Diamond" description="Top Up Diamond" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/phone.png" category="Paket Data" title="Unlimitd Harian 7 Hari" description="Paket Data Smartfren" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/phone.png" category="Paket Data" title="Unlimitd Harian 7 Hari" description="Paket Data Smartfren" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/phone.png" category="Paket Data" title="Unlimitd Harian 7 Hari" description="Paket Data Smartfren" price="28.000" deliverType="Pengiriman Instan" sold={12} />
                <CardProduct width={{ base: 'full' }} href='/product/1' image="/images/products/phone.png" category="Paket Data" title="Unlimitd Harian 7 Hari" description="Paket Data Smartfren" price="28.000" deliverType="Pengiriman Instan" sold={12} />
            </Grid>
            <Box display='flex' justifyContent='center'>
                <Link href='/product' as={NextLink}>
                    <ButtonPrimary size="lg" width="200px">Lihat Semua</ButtonPrimary>
                </Link>
            </Box>
        </Container>
    )
}
