import { Box, Container, Flex } from "@chakra-ui/react";
import { TextTitle } from "../atoms";
import StepItems from "../molecules/StepItems";

export default function Subscribe() {
  return (
    <Box bg='abu-abu' pb='20'>
      <Container maxW='8xl'>
        <Box py='2' textAlign='center'>
          <TextTitle fontSize={{ base: '3xl', md: '4xl', xl: '4xl' }} text='Cara Berlangganan' />

          <Flex alignItems='center' wrap={{ base: 'wrap', md: 'nowrap', xl: 'nowrap' }} justifyContent='center' gap='15' mt='6'>
            <StepItems image="/images/subscribes/subscribe-1.png" step={1} text="Pesan Layanan" />
            <StepItems image="/images/subscribes/subscribe-2.png" step={2} text="Pembayaran" />
            <StepItems image="/images/subscribes/subscribe-3.png" step={3} text="Menunggu Proses" />
            <StepItems image="/images/subscribes/subscribe-4.png" step={4} text="Pesanan Diterima" />
            <StepItems image="/images/subscribes/subscribe-5.png" step={5} text="Selesai!" />
          </Flex>
        </Box>
      </Container>
    </Box>
  )
}
