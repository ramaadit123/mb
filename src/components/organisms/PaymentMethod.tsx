import { Box, Container, Flex, Text } from "@chakra-ui/react";
import { TextTitle } from "../atoms";
import Image from "next/image";

export default function PaymentMethod() {
    return (
        <Box bg='abu-abu' pb={{ base: '10', md: '20', xl: '20' }}>
            <Container maxW={{ base: '8xl', xl: '7xl' }}>
                <Flex mt='10' wrap={{ base: 'wrap', md: 'nowrap', xl: 'nowrap' }}>
                    <Box mt={{ base: '5', md: '0', xl: '0' }}>
                        <TextTitle fontSize={{ base: '3xl', md: '4xl', xl: '4xl' }} text='Metode Pembayaran' mt={{ base: '-10', md: '0', xl: '0' }} />
                        <Text fontSize='xl' fontWeight='semibold' mt='5'>
                            Bertransaksi di Seakun sudah sangat mudah karena dilengkapi dengan beragam metode pembayaran. Mulai dari Virtual Account, E-wallet, QRIS hingga retail.
                        </Text>
                    </Box>
                    <Image
                        src='/images/payments.png'
                        alt='payments'
                        width='750'
                        height='260'
                        style={{ objectFit: 'cover' }}
                    />
                </Flex>
            </Container>
        </Box>
    )
}
