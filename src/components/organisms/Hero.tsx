import { Box, Container } from "@chakra-ui/react"
import Image from "next/image"
import { TextSubtitle, TextTitle } from "../atoms"

export default function Hero() {
    return (
        <Container maxW='8xl' pb={{ base: '10', md: '20', xl: '20' }}>
            <Box display='flex' alignItems='center' justifyContent='center'>
                <Image
                    src='/images/banners/banner.png'
                    alt=''
                    height={334}
                    width={1020}
                />
            </Box>
            <Box textAlign='center'>
                <TextTitle mt={{ base: '5', md: '16', xl: '16' }} fontSize={{ base: '3xl', md: '4xl', xl: '4xl' }} text='Jadilah bagian dari Maubang' />
                <TextSubtitle fontSize={{ base: 'xl', md: '2xl', xl: '2xl' }} text='Dapatkan manfaat Patungan Berlangganan. Nikmati layanan premium dengan lebih hemat, aman dan legal' />
            </Box>
        </Container>
    )
}
