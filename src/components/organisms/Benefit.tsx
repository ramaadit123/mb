import { Box, Container, Flex, Image } from "@chakra-ui/react";
import { BenefitItems, TextTitle } from "../atoms";
import { IconCheck, IconDiscount, IconHeadphone, IconKey, IconMoney, IconNotification, IconPhone } from "../icons";

export default function Benefit() {
    return (
        <>
            <Box bgColor='abu-abu'>
                <Container maxW='8xl'>
                    <Flex gap={{ base: '', md: '16', xl: '16' }} wrap={{ base: 'wrap', md: 'nowrap', xl: 'nowrap' }}>
                        <Image
                            src='/images/vectors/vector-benefit.png'
                            alt=''
                            width={{ base: '500', md: '700', xl: '700' }}
                            height={{ base: '500', md: '700', xl: '700' }}
                            objectFit='cover'
                        />
                        <Box display='block' mt={{ base: '-20', md: '0', xl: '0' }} pb={{ base: '10', md: '0', xl: '0' }}>
                            <TextTitle fontSize={{ base: '3xl', md: '4xl', xl: '4xl' }} text="Beragam manfaat bisa kamu dapatkan disini" />
                            <Box mt='5'>
                                <BenefitItems text='Hemat hingga 70%' icon={<IconDiscount />} />
                                <BenefitItems text='Privasi Terjamin' icon={<IconKey />} />
                                <BenefitItems text='Beragam Metode Pembayaran' icon={<IconMoney />} />
                                <BenefitItems text='Layanan Legal dan Resmi' icon={<IconCheck />} />
                                <BenefitItems text='Pengingat Pembayaran' icon={<IconNotification />} />
                                <BenefitItems text='Customer Service Responsif' icon={<IconHeadphone />} />
                            </Box>
                        </Box>
                    </Flex>
                </Container>
            </Box>

        </>
    )
}
