import { Container, Grid } from "@chakra-ui/react";
import { TextTitle } from "../atoms";
import FaqItems from "../molecules/FaqItems";

export default function Faq() {
    return (
        <Container maxW={{ base: '8xl', xl: '7xl' }} pb={{ base: '10', md: '20', xl: '20' }}>
            <TextTitle fontSize={{ base: '3xl', md: '4xl', xl: '4xl' }} text='Frequently Asked Questions (FAQ)' />

            <Grid templateColumns={{ base: "1fr", md: "1fr 1fr" }} gap={6} pt='10'>
                <FaqItems
                    q='What Is Lorem Ipsum?'
                    a='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat.'
                />
                <FaqItems
                    q='What Is Lorem Ipsum?'
                    a='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat.'
                />
                <FaqItems
                    q='What Is Lorem Ipsum?'
                    a='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat.'
                />
                <FaqItems
                    q='What Is Lorem Ipsum?'
                    a='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat.'
                />
                <FaqItems
                    q='What Is Lorem Ipsum?'
                    a='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat.'
                />
                <FaqItems
                    q='What Is Lorem Ipsum?'
                    a='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
            veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat.'
                />
            </Grid>
        </Container>
    )
}