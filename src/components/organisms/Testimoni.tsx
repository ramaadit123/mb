import { AbsoluteCenter, Box, Container, Divider, Flex, Text } from "@chakra-ui/react";
import { TextTitle } from "../atoms";
import Image from "next/image";
import { IconStar, IconTestArrowRight, IconTestArrowLeft } from "../icons";
import CardTestimoni from "../molecules/CardTestimoni";

export default function Testimoni() {
    return (
        <Container maxW={{ base: '8xl', xl: '7xl' }} pb={{ base: ' 10', md: '20', xl: '20' }}>
            <TextTitle fontSize={{ base: '3xl', md: '4xl', xl: '4xl' }} text="Apa kata mereka tentang maubang?" color='black' />

            <Flex alignItems='center' wrap={{ base: 'wrap', md: 'nowrap', xl: 'nowrap' }} justifyContent='center' mt='8' gap='10' position='relative'>
                <AbsoluteCenter axis='horizontal' left={{ base: '7', xl: '-3' }}>
                    <IconTestArrowLeft />
                </AbsoluteCenter>

                <CardTestimoni image='/images/profiles/profile-1.png' name='Anonymous 1' text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book" />
                <CardTestimoni image='/images/profiles/profile-1.png' name='Anonymous 1' text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book" />
                <CardTestimoni image='/images/profiles/profile-1.png' name='Anonymous 1' text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book" />
                <CardTestimoni image='/images/profiles/profile-1.png' name='Anonymous 1' text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book" />

                <AbsoluteCenter axis='vertical' right={{ base: '0', xl: '-10' }}>
                    <IconTestArrowRight />
                </AbsoluteCenter>
            </Flex>
        </Container>
    )
}
