export interface AppProps {
    title: string,
    children: any
}

export interface HeroImageProps {
    src: string,
    alt: string,
    width: number,
    height: number
}

export interface StepBadgeProps {
    step: number
}

export interface StepTextProps {
    text: string
}

export interface StepItemsProps {
    image: string
    text: string
    step: number

}

export interface TextTitleProps {
    text: string
    color?: string,
    fontSize?: any,
    mt?: any,
}

export interface CardTestimoniProps {
    image: string
    name: string
    text: string
    star?: number
}

export interface FaqItemsProps {
    q: string
    a: string
}

export interface ButtonPrimaryProps {
    width?: {}
    size?: string
    w?: string
    height?: string
    h?: string
    px?: string
    py?: string
    children: any
}

export interface ButtonOulineIconProps {
    rounded?: string
    borderColor?: string
    border?: string
    icon?: any
    mb?: string
    mt?: string
    children: any
    w?: string
    width?: any
    h?: string
    height?: string
    size?: string
    onClick?: () => void
}

export interface NavLinkProps {
    href?: string
    children: any
    fontSize?: {}
    color?: string
    fontWeight?: string
}

export interface CategoryMenuProps {
    href?: string
    name: string
}

export interface CategoryItemsProps {
    image: string
    name: string
    href?: string
}